@extends('layouts.websitepages')
@section('content')
    <!-- Max-Width:1366px; after that content stable in center -->
    <div class="wrapper">
        <!-- Header -->
         @include('menu')
        <!-- // Header -->
        <!--inner banner text -->
        <section class="herbalLibrary">
            <h2 class="animatable fadeInDown">Herbal Library</h2>
            <p class="animatable fadeInUp">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus sodales nunc sed suscipit efficitur. Fusce quis est consectetur, mollis tortor vitae, tempus quam. Vestibulum tincidunt felis nec elit interdum, in convallis odio laoreet. Donec sit amet placerat augue. Praesent et scelerisque ipsum.</p>
        </section>
        <!--//inner banner text -->
        <!--innersearchbar-->
        <section class="selectOption">
            <div class="itemLeft">
                <div class="dropdown selectCategory">
                    <button id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        All Categories
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" aria-labelledby="dLabel">
                        <li><a href="all-product.html">Herbs</a></li>
                        <li><a href="all-product.html">Shrubs</a></li>
                        <li><a href="all-product.html">Trees (Sapling)</a></li>
                        <li><a href="all-product.html">Climbers</a></li>
                        <li><a href="all-product.html">Fern</a></li>
                        <li><a href="all-product.html">Grass</a></li>
                        <li><a href="all-product.html">Epiphytic Parasite</a></li>
                        <li><a href="all-product.html">Epiphytic</a></li>
                        <li><a href="all-product.html">Liana</a></li>
                    </ul>
                </div>
                <div class="searchProduct">
                    <input type="search" placeholder="SEARCH FOR TREES (SAPLING)">
                </div>
            </div>
            <div class="itemRight">
                <div class="dropdown sort">
                    <button id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Sort by popularity
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" aria-labelledby="dLabel">
                        <li><a href="#">Sort by Latest</a></li>
                        <li><a href="#">Sort by A to Z</a></li>
                        <li><a href="#">Sort by Z to A</a></li>
                    </ul>
                </div>
            </div>
        </section>
        <!--//innersearchbar-->
        <!--category -->
        <section class="categories">
            <h4 class="animatable fadeInDown">Categories</h4>
            <div class="category-section">
                <div class="row">
                    @foreach($category as $categories)
                    <div class="col-sm-6 animatable fadeInUp">
                        <figure>
                            <a href="all-product.html" class="proImg">
                                <img src="{{url('/')}}/{{$categories->category_image}}" alt="">
                                <h4 class="animatable fadeInUp">{{$categories->category_name}}</h4>
                            </a>
                        </figure>
                    </div>
                    @endforeach
                   
                </div>
               
            </div>
        </section>
        <!-- //category -->
        <!-- Popular products -->
        <section id="allproducts" class="allproducts">
            <h4 class="animatable fadeInDown">Products</h4>
            <div class="row">
                <div class="col-sm-4 animatable fadeInUp">
                    <figure style="background: url(images/proImg1.jpg)">
                        <a href="JavaScript:void(0)" class="quickView" data-toggle="modal" data-target="#QuickViewModal">
                            <span>
                            <svg width="100" height="100" viewBox="0 0 30 30" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg">
                                <defs>
                                    <path d="M0,15.089434 C0,16.3335929 5.13666091,24.1788679 14.9348958,24.1788679 C24.7325019,24.1788679 29.8697917,16.3335929 29.8697917,15.089434 C29.8697917,13.8456167 24.7325019,6 14.9348958,6 C5.13666091,6 0,13.8456167 0,15.089434 Z" id="outline"></path>
                                    <mask id="mask">
                                        <rect width="100%" height="100%" fill="white"></rect>
                                        <use xlink:href="#outline" id="lid" fill="black" />
                                    </mask>
                                </defs>
                                <g id="eye">
                                    <path d="M0,15.089434 C0,16.3335929 5.13666091,24.1788679 14.9348958,24.1788679 C24.7325019,24.1788679 29.8697917,16.3335929 29.8697917,15.089434 C29.8697917,13.8456167 24.7325019,6 14.9348958,6 C5.13666091,6 0,13.8456167 0,15.089434 Z M14.9348958,22.081464 C11.2690863,22.081464 8.29688487,18.9510766 8.29688487,15.089434 C8.29688487,11.2277914 11.2690863,8.09740397 14.9348958,8.09740397 C18.6007053,8.09740397 21.5725924,11.2277914 21.5725924,15.089434 C21.5725924,18.9510766 18.6007053,22.081464 14.9348958,22.081464 L14.9348958,22.081464 Z M18.2535869,15.089434 C18.2535869,17.0200844 16.7673289,18.5857907 14.9348958,18.5857907 C13.1018339,18.5857907 11.6162048,17.0200844 11.6162048,15.089434 C11.6162048,13.1587835 13.1018339,11.593419 14.9348958,11.593419 C15.9253152,11.593419 14.3271242,14.3639878 14.9348958,15.089434 C15.451486,15.7055336 18.2535869,14.2027016 18.2535869,15.089434 L18.2535869,15.089434 Z" fill="#fff"></path>
                                    <use xlink:href="#outline" mask="url(#mask)" fill="#fff" />
                                </g>
                            </svg>
                            <br> QUICK VIEW
                            </span>
                        </a>
                    </figure>
                    <a href="product.html">
                        <h4>Melaleuca alternifolia</h4>
                        <h5>Bahera /Behada/Vibhitaka</h5>
                    </a>
                </div>
                <div class="col-sm-4 animatable fadeInUp">
                    <figure style="background: url(images/proImg2.jpg)">
                        <a href="JavaScript:void(0)" class="quickView" data-toggle="modal" data-target="#QuickViewModal">
                            <span>
                            <svg width="100" height="100" viewBox="0 0 30 30" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg">
                                <defs>
                                    <path d="M0,15.089434 C0,16.3335929 5.13666091,24.1788679 14.9348958,24.1788679 C24.7325019,24.1788679 29.8697917,16.3335929 29.8697917,15.089434 C29.8697917,13.8456167 24.7325019,6 14.9348958,6 C5.13666091,6 0,13.8456167 0,15.089434 Z" id="outline"></path>
                                    <mask id="mask">
                                        <rect width="100%" height="100%" fill="white"></rect>
                                        <use xlink:href="#outline" id="lid" fill="black" />
                                    </mask>
                                </defs>
                                <g id="eye">
                                    <path d="M0,15.089434 C0,16.3335929 5.13666091,24.1788679 14.9348958,24.1788679 C24.7325019,24.1788679 29.8697917,16.3335929 29.8697917,15.089434 C29.8697917,13.8456167 24.7325019,6 14.9348958,6 C5.13666091,6 0,13.8456167 0,15.089434 Z M14.9348958,22.081464 C11.2690863,22.081464 8.29688487,18.9510766 8.29688487,15.089434 C8.29688487,11.2277914 11.2690863,8.09740397 14.9348958,8.09740397 C18.6007053,8.09740397 21.5725924,11.2277914 21.5725924,15.089434 C21.5725924,18.9510766 18.6007053,22.081464 14.9348958,22.081464 L14.9348958,22.081464 Z M18.2535869,15.089434 C18.2535869,17.0200844 16.7673289,18.5857907 14.9348958,18.5857907 C13.1018339,18.5857907 11.6162048,17.0200844 11.6162048,15.089434 C11.6162048,13.1587835 13.1018339,11.593419 14.9348958,11.593419 C15.9253152,11.593419 14.3271242,14.3639878 14.9348958,15.089434 C15.451486,15.7055336 18.2535869,14.2027016 18.2535869,15.089434 L18.2535869,15.089434 Z" fill="#fff"></path>
                                    <use xlink:href="#outline" mask="url(#mask)" fill="#fff" />
                                </g>
                            </svg>
                            <br> QUICK VIEW
                            </span>
                        </a>
                    </figure>
                    <a href="product.html">
                        <h4>Melaleuca alternifolia</h4>
                        <h5>Bahera /Behada/Vibhitaka</h5>
                    </a>
                </div>
                <div class="col-sm-4 animatable fadeInUp">
                    <figure style="background: url(images/proImg3.jpg)">
                        <a href="JavaScript:void(0)" class="quickView" data-toggle="modal" data-target="#QuickViewModal">
                            <span>
                            <svg width="100" height="100" viewBox="0 0 30 30" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg">
                                <defs>
                                    <path d="M0,15.089434 C0,16.3335929 5.13666091,24.1788679 14.9348958,24.1788679 C24.7325019,24.1788679 29.8697917,16.3335929 29.8697917,15.089434 C29.8697917,13.8456167 24.7325019,6 14.9348958,6 C5.13666091,6 0,13.8456167 0,15.089434 Z" id="outline"></path>
                                    <mask id="mask">
                                        <rect width="100%" height="100%" fill="white"></rect>
                                        <use xlink:href="#outline" id="lid" fill="black" />
                                    </mask>
                                </defs>
                                <g id="eye">
                                    <path d="M0,15.089434 C0,16.3335929 5.13666091,24.1788679 14.9348958,24.1788679 C24.7325019,24.1788679 29.8697917,16.3335929 29.8697917,15.089434 C29.8697917,13.8456167 24.7325019,6 14.9348958,6 C5.13666091,6 0,13.8456167 0,15.089434 Z M14.9348958,22.081464 C11.2690863,22.081464 8.29688487,18.9510766 8.29688487,15.089434 C8.29688487,11.2277914 11.2690863,8.09740397 14.9348958,8.09740397 C18.6007053,8.09740397 21.5725924,11.2277914 21.5725924,15.089434 C21.5725924,18.9510766 18.6007053,22.081464 14.9348958,22.081464 L14.9348958,22.081464 Z M18.2535869,15.089434 C18.2535869,17.0200844 16.7673289,18.5857907 14.9348958,18.5857907 C13.1018339,18.5857907 11.6162048,17.0200844 11.6162048,15.089434 C11.6162048,13.1587835 13.1018339,11.593419 14.9348958,11.593419 C15.9253152,11.593419 14.3271242,14.3639878 14.9348958,15.089434 C15.451486,15.7055336 18.2535869,14.2027016 18.2535869,15.089434 L18.2535869,15.089434 Z" fill="#fff"></path>
                                    <use xlink:href="#outline" mask="url(#mask)" fill="#fff" />
                                </g>
                            </svg>
                            <br> QUICK VIEW
                            </span>
                        </a>
                    </figure>
                    <a href="product.html">
                        <h4>Melaleuca alternifolia</h4>
                        <h5>Bahera /Behada/Vibhitaka</h5>
                    </a>
                </div>
                <div class="col-sm-4 animatable fadeInUp">
                    <figure style="background: url(images/proImg4.jpg)">
                        <a href="JavaScript:void(0)" class="quickView" data-toggle="modal" data-target="#QuickViewModal">
                            <span>
                            <svg width="100" height="100" viewBox="0 0 30 30" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg">
                                <defs>
                                    <path d="M0,15.089434 C0,16.3335929 5.13666091,24.1788679 14.9348958,24.1788679 C24.7325019,24.1788679 29.8697917,16.3335929 29.8697917,15.089434 C29.8697917,13.8456167 24.7325019,6 14.9348958,6 C5.13666091,6 0,13.8456167 0,15.089434 Z" id="outline"></path>
                                    <mask id="mask">
                                        <rect width="100%" height="100%" fill="white"></rect>
                                        <use xlink:href="#outline" id="lid" fill="black" />
                                    </mask>
                                </defs>
                                <g id="eye">
                                    <path d="M0,15.089434 C0,16.3335929 5.13666091,24.1788679 14.9348958,24.1788679 C24.7325019,24.1788679 29.8697917,16.3335929 29.8697917,15.089434 C29.8697917,13.8456167 24.7325019,6 14.9348958,6 C5.13666091,6 0,13.8456167 0,15.089434 Z M14.9348958,22.081464 C11.2690863,22.081464 8.29688487,18.9510766 8.29688487,15.089434 C8.29688487,11.2277914 11.2690863,8.09740397 14.9348958,8.09740397 C18.6007053,8.09740397 21.5725924,11.2277914 21.5725924,15.089434 C21.5725924,18.9510766 18.6007053,22.081464 14.9348958,22.081464 L14.9348958,22.081464 Z M18.2535869,15.089434 C18.2535869,17.0200844 16.7673289,18.5857907 14.9348958,18.5857907 C13.1018339,18.5857907 11.6162048,17.0200844 11.6162048,15.089434 C11.6162048,13.1587835 13.1018339,11.593419 14.9348958,11.593419 C15.9253152,11.593419 14.3271242,14.3639878 14.9348958,15.089434 C15.451486,15.7055336 18.2535869,14.2027016 18.2535869,15.089434 L18.2535869,15.089434 Z" fill="#fff"></path>
                                    <use xlink:href="#outline" mask="url(#mask)" fill="#fff" />
                                </g>
                            </svg>
                            <br> QUICK VIEW
                            </span>
                        </a>
                    </figure>
                    <a href="product.html">
                        <h4>Melaleuca alternifolia</h4>
                        <h5>Bahera /Behada/Vibhitaka</h5>
                    </a>
                </div>
                <div class="col-sm-4 animatable fadeInUp">
                    <figure style="background: url(images/proImg5.jpg)">
                        <a href="JavaScript:void(0)" class="quickView" data-toggle="modal" data-target="#QuickViewModal">
                            <span>
                            <svg width="100" height="100" viewBox="0 0 30 30" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg">
                                <defs>
                                    <path d="M0,15.089434 C0,16.3335929 5.13666091,24.1788679 14.9348958,24.1788679 C24.7325019,24.1788679 29.8697917,16.3335929 29.8697917,15.089434 C29.8697917,13.8456167 24.7325019,6 14.9348958,6 C5.13666091,6 0,13.8456167 0,15.089434 Z" id="outline"></path>
                                    <mask id="mask">
                                        <rect width="100%" height="100%" fill="white"></rect>
                                        <use xlink:href="#outline" id="lid" fill="black" />
                                    </mask>
                                </defs>
                                <g id="eye">
                                    <path d="M0,15.089434 C0,16.3335929 5.13666091,24.1788679 14.9348958,24.1788679 C24.7325019,24.1788679 29.8697917,16.3335929 29.8697917,15.089434 C29.8697917,13.8456167 24.7325019,6 14.9348958,6 C5.13666091,6 0,13.8456167 0,15.089434 Z M14.9348958,22.081464 C11.2690863,22.081464 8.29688487,18.9510766 8.29688487,15.089434 C8.29688487,11.2277914 11.2690863,8.09740397 14.9348958,8.09740397 C18.6007053,8.09740397 21.5725924,11.2277914 21.5725924,15.089434 C21.5725924,18.9510766 18.6007053,22.081464 14.9348958,22.081464 L14.9348958,22.081464 Z M18.2535869,15.089434 C18.2535869,17.0200844 16.7673289,18.5857907 14.9348958,18.5857907 C13.1018339,18.5857907 11.6162048,17.0200844 11.6162048,15.089434 C11.6162048,13.1587835 13.1018339,11.593419 14.9348958,11.593419 C15.9253152,11.593419 14.3271242,14.3639878 14.9348958,15.089434 C15.451486,15.7055336 18.2535869,14.2027016 18.2535869,15.089434 L18.2535869,15.089434 Z" fill="#fff"></path>
                                    <use xlink:href="#outline" mask="url(#mask)" fill="#fff" />
                                </g>
                            </svg>
                            <br> QUICK VIEW
                            </span>
                        </a>
                    </figure>
                    <a href="product.html">
                        <h4>Melaleuca alternifolia</h4>
                        <h5>Bahera /Behada/Vibhitaka</h5>
                    </a>
                </div>
                <div class="col-sm-4 animatable fadeInUp">
                    <figure style="background: url(images/proImg6.jpg)">
                        <a href="JavaScript:void(0)" class="quickView" data-toggle="modal" data-target="#QuickViewModal">
                            <span>
                            <svg width="100" height="100" viewBox="0 0 30 30" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg">
                                <defs>
                                    <path d="M0,15.089434 C0,16.3335929 5.13666091,24.1788679 14.9348958,24.1788679 C24.7325019,24.1788679 29.8697917,16.3335929 29.8697917,15.089434 C29.8697917,13.8456167 24.7325019,6 14.9348958,6 C5.13666091,6 0,13.8456167 0,15.089434 Z" id="outline"></path>
                                    <mask id="mask">
                                        <rect width="100%" height="100%" fill="white"></rect>
                                        <use xlink:href="#outline" id="lid" fill="black" />
                                    </mask>
                                </defs>
                                <g id="eye">
                                    <path d="M0,15.089434 C0,16.3335929 5.13666091,24.1788679 14.9348958,24.1788679 C24.7325019,24.1788679 29.8697917,16.3335929 29.8697917,15.089434 C29.8697917,13.8456167 24.7325019,6 14.9348958,6 C5.13666091,6 0,13.8456167 0,15.089434 Z M14.9348958,22.081464 C11.2690863,22.081464 8.29688487,18.9510766 8.29688487,15.089434 C8.29688487,11.2277914 11.2690863,8.09740397 14.9348958,8.09740397 C18.6007053,8.09740397 21.5725924,11.2277914 21.5725924,15.089434 C21.5725924,18.9510766 18.6007053,22.081464 14.9348958,22.081464 L14.9348958,22.081464 Z M18.2535869,15.089434 C18.2535869,17.0200844 16.7673289,18.5857907 14.9348958,18.5857907 C13.1018339,18.5857907 11.6162048,17.0200844 11.6162048,15.089434 C11.6162048,13.1587835 13.1018339,11.593419 14.9348958,11.593419 C15.9253152,11.593419 14.3271242,14.3639878 14.9348958,15.089434 C15.451486,15.7055336 18.2535869,14.2027016 18.2535869,15.089434 L18.2535869,15.089434 Z" fill="#fff"></path>
                                    <use xlink:href="#outline" mask="url(#mask)" fill="#fff" />
                                </g>
                            </svg>
                            <br> QUICK VIEW
                            </span>
                        </a>
                    </figure>
                    <a href="product.html">
                        <h4>Melaleuca alternifolia</h4>
                        <h5>Bahera /Behada/Vibhitaka</h5>
                    </a>
                </div>
                <div class="col-sm-4 animatable fadeInUp">
                    <figure style="background: url(images/proImg7.jpg)">
                        <a href="JavaScript:void(0)" class="quickView" data-toggle="modal" data-target="#QuickViewModal">
                            <span>
                            <svg width="100" height="100" viewBox="0 0 30 30" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg">
                                <defs>
                                    <path d="M0,15.089434 C0,16.3335929 5.13666091,24.1788679 14.9348958,24.1788679 C24.7325019,24.1788679 29.8697917,16.3335929 29.8697917,15.089434 C29.8697917,13.8456167 24.7325019,6 14.9348958,6 C5.13666091,6 0,13.8456167 0,15.089434 Z" id="outline"></path>
                                    <mask id="mask">
                                        <rect width="100%" height="100%" fill="white"></rect>
                                        <use xlink:href="#outline" id="lid" fill="black" />
                                    </mask>
                                </defs>
                                <g id="eye">
                                    <path d="M0,15.089434 C0,16.3335929 5.13666091,24.1788679 14.9348958,24.1788679 C24.7325019,24.1788679 29.8697917,16.3335929 29.8697917,15.089434 C29.8697917,13.8456167 24.7325019,6 14.9348958,6 C5.13666091,6 0,13.8456167 0,15.089434 Z M14.9348958,22.081464 C11.2690863,22.081464 8.29688487,18.9510766 8.29688487,15.089434 C8.29688487,11.2277914 11.2690863,8.09740397 14.9348958,8.09740397 C18.6007053,8.09740397 21.5725924,11.2277914 21.5725924,15.089434 C21.5725924,18.9510766 18.6007053,22.081464 14.9348958,22.081464 L14.9348958,22.081464 Z M18.2535869,15.089434 C18.2535869,17.0200844 16.7673289,18.5857907 14.9348958,18.5857907 C13.1018339,18.5857907 11.6162048,17.0200844 11.6162048,15.089434 C11.6162048,13.1587835 13.1018339,11.593419 14.9348958,11.593419 C15.9253152,11.593419 14.3271242,14.3639878 14.9348958,15.089434 C15.451486,15.7055336 18.2535869,14.2027016 18.2535869,15.089434 L18.2535869,15.089434 Z" fill="#fff"></path>
                                    <use xlink:href="#outline" mask="url(#mask)" fill="#fff" />
                                </g>
                            </svg>
                            <br> QUICK VIEW
                            </span>
                        </a>
                    </figure>
                    <a href="product.html">
                        <h4>Melaleuca alternifolia</h4>
                        <h5>Bahera /Behada/Vibhitaka</h5>
                    </a>
                </div>
                <div class="col-sm-4 animatable fadeInUp">
                    <figure style="background: url(images/proImg8.jpg)">
                        <a href="JavaScript:void(0)" class="quickView" data-toggle="modal" data-target="#QuickViewModal">
                            <span>
                            <svg width="100" height="100" viewBox="0 0 30 30" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg">
                                <defs>
                                    <path d="M0,15.089434 C0,16.3335929 5.13666091,24.1788679 14.9348958,24.1788679 C24.7325019,24.1788679 29.8697917,16.3335929 29.8697917,15.089434 C29.8697917,13.8456167 24.7325019,6 14.9348958,6 C5.13666091,6 0,13.8456167 0,15.089434 Z" id="outline"></path>
                                    <mask id="mask">
                                        <rect width="100%" height="100%" fill="white"></rect>
                                        <use xlink:href="#outline" id="lid" fill="black" />
                                    </mask>
                                </defs>
                                <g id="eye">
                                    <path d="M0,15.089434 C0,16.3335929 5.13666091,24.1788679 14.9348958,24.1788679 C24.7325019,24.1788679 29.8697917,16.3335929 29.8697917,15.089434 C29.8697917,13.8456167 24.7325019,6 14.9348958,6 C5.13666091,6 0,13.8456167 0,15.089434 Z M14.9348958,22.081464 C11.2690863,22.081464 8.29688487,18.9510766 8.29688487,15.089434 C8.29688487,11.2277914 11.2690863,8.09740397 14.9348958,8.09740397 C18.6007053,8.09740397 21.5725924,11.2277914 21.5725924,15.089434 C21.5725924,18.9510766 18.6007053,22.081464 14.9348958,22.081464 L14.9348958,22.081464 Z M18.2535869,15.089434 C18.2535869,17.0200844 16.7673289,18.5857907 14.9348958,18.5857907 C13.1018339,18.5857907 11.6162048,17.0200844 11.6162048,15.089434 C11.6162048,13.1587835 13.1018339,11.593419 14.9348958,11.593419 C15.9253152,11.593419 14.3271242,14.3639878 14.9348958,15.089434 C15.451486,15.7055336 18.2535869,14.2027016 18.2535869,15.089434 L18.2535869,15.089434 Z" fill="#fff"></path>
                                    <use xlink:href="#outline" mask="url(#mask)" fill="#fff" />
                                </g>
                            </svg>
                            <br> QUICK VIEW
                            </span>
                        </a>
                    </figure>
                    <a href="product.html">
                        <h4>Melaleuca alternifolia</h4>
                        <h5>Bahera /Behada/Vibhitaka</h5>
                    </a>
                </div>
                <div class="col-sm-4 animatable fadeInUp">
                    <figure style="background: url(images/proImg9.jpg)">
                        <a href="JavaScript:void(0)" class="quickView" data-toggle="modal" data-target="#QuickViewModal">
                            <span>
                            <svg width="100" height="100" viewBox="0 0 30 30" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg">
                                <defs>
                                    <path d="M0,15.089434 C0,16.3335929 5.13666091,24.1788679 14.9348958,24.1788679 C24.7325019,24.1788679 29.8697917,16.3335929 29.8697917,15.089434 C29.8697917,13.8456167 24.7325019,6 14.9348958,6 C5.13666091,6 0,13.8456167 0,15.089434 Z" id="outline"></path>
                                    <mask id="mask">
                                        <rect width="100%" height="100%" fill="white"></rect>
                                        <use xlink:href="#outline" id="lid" fill="black" />
                                    </mask>
                                </defs>
                                <g id="eye">
                                    <path d="M0,15.089434 C0,16.3335929 5.13666091,24.1788679 14.9348958,24.1788679 C24.7325019,24.1788679 29.8697917,16.3335929 29.8697917,15.089434 C29.8697917,13.8456167 24.7325019,6 14.9348958,6 C5.13666091,6 0,13.8456167 0,15.089434 Z M14.9348958,22.081464 C11.2690863,22.081464 8.29688487,18.9510766 8.29688487,15.089434 C8.29688487,11.2277914 11.2690863,8.09740397 14.9348958,8.09740397 C18.6007053,8.09740397 21.5725924,11.2277914 21.5725924,15.089434 C21.5725924,18.9510766 18.6007053,22.081464 14.9348958,22.081464 L14.9348958,22.081464 Z M18.2535869,15.089434 C18.2535869,17.0200844 16.7673289,18.5857907 14.9348958,18.5857907 C13.1018339,18.5857907 11.6162048,17.0200844 11.6162048,15.089434 C11.6162048,13.1587835 13.1018339,11.593419 14.9348958,11.593419 C15.9253152,11.593419 14.3271242,14.3639878 14.9348958,15.089434 C15.451486,15.7055336 18.2535869,14.2027016 18.2535869,15.089434 L18.2535869,15.089434 Z" fill="#fff"></path>
                                    <use xlink:href="#outline" mask="url(#mask)" fill="#fff" />
                                </g>
                            </svg>
                            <br> QUICK VIEW
                            </span>
                        </a>
                    </figure>
                    <a href="product.html">
                        <h4>Melaleuca alternifolia</h4>
                        <h5>Bahera /Behada/Vibhitaka</h5>
                    </a>
                </div>
            </div>
        </section>
    </div>
    <!-- Pagination -->
    <section class="customPagination animatable fadeInDown">
        <div class="row">
            <div class="col-lg-12">
                <ul class="list-unstyled">
                    <li>
                        <a href="#">
                            <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="856.124px" height="289.453px" viewBox="413.163 395.273 856.124 289.453" enable-background="new 413.163 395.273 856.124 289.453" xml:space="preserve">
                                <g>
                                    <rect x="524.542" y="521.695" width="744.745" height="36.609" />
                                    <polygon points="663.836,684.727 413.163,540 663.836,395.273    " />
                                </g>
                            </svg> First</a>
                    </li>
                    <li>
                        <a href="#">
                            <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="856.124px" height="289.453px" viewBox="413.163 395.273 856.124 289.453" enable-background="new 413.163 395.273 856.124 289.453" xml:space="preserve">
                                <g>
                                    <rect x="524.542" y="521.695" width="744.745" height="36.609" />
                                    <polygon points="663.836,684.727 413.163,540 663.836,395.273    " />
                                </g>
                            </svg>
                        </a>
                    </li>
                    <li class="active"><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">...</a></li>
                    <li>
                        <a href="#" class="flip">
                            <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="856.124px" height="289.453px" viewBox="413.163 395.273 856.124 289.453" enable-background="new 413.163 395.273 856.124 289.453" xml:space="preserve">
                                <g>
                                    <rect x="524.542" y="521.695" width="744.745" height="36.609" />
                                    <polygon points="663.836,684.727 413.163,540 663.836,395.273    " />
                                </g>
                            </svg>
                        </a>
                    </li>
                    <li><a href="#" class="flip">Last <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
     width="856.124px" height="289.453px" viewBox="413.163 395.273 856.124 289.453"
     enable-background="new 413.163 395.273 856.124 289.453" xml:space="preserve">
<g>
    <rect x="524.542" y="521.695" width="744.745" height="36.609"/>
    <polygon points="663.836,684.727 413.163,540 663.836,395.273    "/>
</g>
</svg></a></li>
                </ul>
            </div>
        </div>
    </section>
    
    @endsection
   
  