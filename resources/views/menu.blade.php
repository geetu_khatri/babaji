<nav class="navbar navbar-default">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a href="#" class="searchBtn">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="279 224 250.312 250.312">
                            <defs>
                                <style>
                                .a {
                                    fill-rule: evenodd;
                                }
                                </style>
                            </defs>
                            <g transform="translate(279 224)">
                                <path class="a" d="M244.186,214.6l-54.379-54.378c-.289-.289-.628-.491-.93-.76a102.945,102.945,0,1,0-29.413,29.411c.269.3.47.64.759.929l54.38,54.38A20.918,20.918,0,1,0,244.186,214.6ZM102.911,170.146a67.236,67.236,0,1,1,67.235-67.235A67.235,67.235,0,0,1,102.911,170.146Z" />
                            </g>
                        </svg>
                    </a>
                    <a class="navbar-brand" href="index.html">
                        <span class="logoWeb"><img src="{{asset('website/images/logoWeb.png')}}" alt=""></span>
                        <span class="logoMob"><img src="{{asset('website/images/logoMob.png')}}" alt=""></span>
                    </a>
                    <div class="forestLogo"><img src="{{asset('website/images/whfLogo.png')}}" alt=""></div>
                    <div class="haryanaGovtLogo">
                        <img src="{{asset('website/images/haryanaGovtLogo1.png')}}" alt="">
                        <img src="{{asset('website/images/haryanaGovtLogo2.png')}}" alt="">
                    </div>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li><a href="360.html">Experience</a></li>
                        <li class="dropdown">
                            <a href="category.html" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Vegetation <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="{{url('/')}}/category">All Vegetation</a></li>
                                <li><a href="all-product.html">Herbs</a></li>
                                <li><a href="all-product.html">Shrubs</a></li>
                                <li><a href="all-product.html">Trees (Sapling)</a></li>
                                <li><a href="all-product.html">Climbers</a></li>
                                <li><a href="all-product.html">Fern</a></li>
                                <li><a href="all-product.html">Grass</a></li>
                                <li><a href="all-product.html">Epiphytic Parasite</a></li>
                                <li><a href="all-product.html">Epiphytic</a></li>
                                <li><a href="all-product.html">Liana</a></li>
                            </ul>
                        </li>
                        <li><a href="#">Forest Safari</a></li>
                        <li><a href="#">Research center</a></li>
                        <li><a href="#">News</a></li>
                        <li><a href="#">About</a></li>
                        <li><a href="#">Contact</a></li>
                        <li class="search">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Search 
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="279 224 250.312 250.312"><defs><style>.a{fill-rule:evenodd;}</style></defs><g transform="translate(279 224)"><path class="a" d="M244.186,214.6l-54.379-54.378c-.289-.289-.628-.491-.93-.76a102.945,102.945,0,1,0-29.413,29.411c.269.3.47.64.759.929l54.38,54.38A20.918,20.918,0,1,0,244.186,214.6ZM102.911,170.146a67.236,67.236,0,1,1,67.235-67.235A67.235,67.235,0,0,1,102.911,170.146Z"/></g></svg></a>
                        </li>
                    </ul>
                    <!-- Search Container -->
                    <div class="searchContainer">
                        <div class="searchContent">
                            <div class="searchFlex">
                                <input type="text" name="" placeholder="Search Here">
                                <button>
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="279 224 250.312 250.312">
                                        <defs>
                                            <style>
                                            .a {
                                                fill-rule: evenodd;
                                            }
                                            </style>
                                        </defs>
                                        <g transform="translate(279 224)">
                                            <path class="a" d="M244.186,214.6l-54.379-54.378c-.289-.289-.628-.491-.93-.76a102.945,102.945,0,1,0-29.413,29.411c.269.3.47.64.759.929l54.38,54.38A20.918,20.918,0,1,0,244.186,214.6ZM102.911,170.146a67.236,67.236,0,1,1,67.235-67.235A67.235,67.235,0,0,1,102.911,170.146Z" />
                                        </g>
                                    </svg>
                                </button>
                                <button class="times">
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="389 332 31.112 31.112">
                                        <path d="M31.112,1.414,29.7,0,15.556,14.142,1.414,0,0,1.414,14.142,15.556,0,29.7l1.414,1.414L15.556,16.97,29.7,31.112,31.112,29.7,16.97,15.556Z" transform="translate(389 332)" />
                                    </svg>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.navbar-collapse -->
            </div>
            <!-- /.container-fluid -->
        </nav>