@extends('layouts.websitepages')
@section('content')
<div class="wrapper homePage">
        <!-- Header -->
        @include('menu')
        <!-- // Header -->
        <!-- Return to nature -->
        <div id="homeCarousel" class="carousel slide carousel-fade" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#homeCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#homeCarousel" data-slide-to="1"></li>
            </ol>
            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                <?php  $num = 0;?>
                  @foreach($slider as $sliders)
                 
                <div class="item <?php if($num==0){ ?>active<?php } ?>">
                    <section class="returnToNature parallax1" style="background:url({{url('/')}}/{{ $sliders->imagename}})">
                        <div class="content">
                            <h1 class="animatable fadeInDown">return to nature</h1>
                            <a href="360.html" class=" animatable fadeInUp">
                                <figure>
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="148 156 512.001 383.16">
                                        <g transform="translate(148 91.58)">
                                            <path d="M381.747,210.816a57.138,57.138,0,1,0,57.138,57.138A57.2,57.2,0,0,0,381.747,210.816Z" />
                                            <path d="M130.253,210.816a57.138,57.138,0,1,0,57.138,57.138A57.2,57.2,0,0,0,130.253,210.816Z" />
                                            <path d="M495.2,136.123H461.719v-54.9a16.805,16.805,0,0,0-16.8-16.805H67.087A16.805,16.805,0,0,0,50.282,81.225v54.9H16.805A16.807,16.807,0,0,0,0,152.928V430.775a16.805,16.805,0,0,0,16.805,16.8H203.828A16.808,16.808,0,0,0,220.4,433.546l8.059-48.192a26.231,26.231,0,0,1,25.951-21.972h3.173a26.232,26.232,0,0,1,25.951,21.972l8.059,48.192a16.807,16.807,0,0,0,16.576,14.034H495.2a16.805,16.805,0,0,0,16.8-16.8V152.928A16.807,16.807,0,0,0,495.2,136.123ZM130.253,358.7A90.748,90.748,0,1,1,221,267.954,90.851,90.851,0,0,1,130.253,358.7Zm251.494,0a90.748,90.748,0,1,1,90.748-90.748A90.851,90.851,0,0,1,381.747,358.7Z" />
                                        </g>
                                    </svg>
                                </figure>
                                Experience
                            </a>
                            <a href="#" class="youtube youtubeClick animatable fadeInUp">
                                <figure>
                                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 310 310" style="enable-background:new 0 0 310 310;" xml:space="preserve">
                                        <g id="XMLID_822_">
                                            <path id="XMLID_823_" d="M297.917,64.645c-11.19-13.302-31.85-18.728-71.306-18.728H83.386c-40.359,0-61.369,5.776-72.517,19.938
        C0,79.663,0,100.008,0,128.166v53.669c0,54.551,12.896,82.248,83.386,82.248h143.226c34.216,0,53.176-4.788,65.442-16.527
        C304.633,235.518,310,215.863,310,181.835v-53.669C310,98.471,309.159,78.006,297.917,64.645z M199.021,162.41l-65.038,33.991
        c-1.454,0.76-3.044,1.137-4.632,1.137c-1.798,0-3.592-0.484-5.181-1.446c-2.992-1.813-4.819-5.056-4.819-8.554v-67.764
        c0-3.492,1.822-6.732,4.808-8.546c2.987-1.814,6.702-1.938,9.801-0.328l65.038,33.772c3.309,1.718,5.387,5.134,5.392,8.861
        C204.394,157.263,202.325,160.684,199.021,162.41z" />
                                        </g>
                                    </svg>
                                </figure>
                                Watch Video
                            </a>
                        </div>
                    </section>
                </div>
             <?php $num++;?>     
                  @endforeach
            
            </div>
        </div>
        <!-- // Return to nature -->
        <!-- Tour Slider -->
        <section class="tourSlider">
            <div id="carousel-example-generic" class="carousel carousel-fade slide" data-ride="carousel">
                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                     <?php  $num = 0;?>
                    @foreach($tours as $tour)
                    <div class="item <?php if($num==0){ ?>active<?php } ?>">
                        <div class="tourContent">
                            <div class="col-sm-6 tourInfo">
                                <h2 class="animatable fadeInDown">{{$tour->title}}</h2>
                                <p class="animatable fadeInUp">{{$tour->description}}</p>
                                <div class="timming animatable fadeInDown">
                                    <img src="images/clock.svg" alt=""> {{$tour->time}}</div>
                            </div>
                            <div class="col-sm-6 tourImage animatable fadeInDown">
                                <a href="{{url('/')}}/360view/{{$tour->id}}" class=" animatable bounceIn">
                                    <figure>
                                        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                            <g>
                                                <path d="M381.747,210.816c-31.505,0-57.138,25.631-57.138,57.138c0,31.506,25.633,57.138,57.138,57.138
            s57.138-25.631,57.138-57.138C438.885,236.448,413.252,210.816,381.747,210.816z" />
                                            </g>
                                            <g>
                                                <path d="M130.253,210.816c-31.505,0-57.138,25.631-57.138,57.138c0,31.506,25.633,57.138,57.138,57.138
            s57.138-25.631,57.138-57.138C187.391,236.448,161.758,210.816,130.253,210.816z" />
                                            </g>
                                            <g>
                                                <path d="M495.195,136.123h-33.476V81.225c0-9.281-7.524-16.805-16.805-16.805H67.087c-9.281,0-16.805,7.524-16.805,16.805v54.897
            H16.805C7.524,136.123,0,143.647,0,152.928v277.847c0,9.281,7.524,16.805,16.805,16.805h187.023
            c8.211,0,15.221-5.935,16.576-14.034l8.059-48.192c2.129-12.732,13.043-21.972,25.951-21.972h3.173
            c12.908,0,23.822,9.241,25.951,21.972l8.059,48.192c1.354,8.099,8.365,14.034,16.576,14.034h187.023
            c9.281,0,16.805-7.524,16.805-16.805V152.928C512,143.647,504.476,136.123,495.195,136.123z M130.253,358.702
            c-50.038,0-90.748-40.709-90.748-90.748s40.71-90.748,90.748-90.748c50.038,0,90.748,40.709,90.748,90.748
            S180.291,358.702,130.253,358.702z M381.747,358.702c-50.038,0-90.748-40.709-90.748-90.748s40.71-90.748,90.748-90.748
            s90.748,40.709,90.748,90.748S431.785,358.702,381.747,358.702z" />
                                            </g>
                                        </svg>
                                    </figure>
                                </a>
                                <img src="{{url('/')}}/{{$tour->tour_image}}" alt="">
                            </div>
                        </div>
                    </div>
                     <?php $num++;?>    
                    @endforeach
                </div>
                <!-- Controls -->
                <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                    <img src="{{url('/')}}/public/website/images/arrow.svg" alt="">
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                    <img src="{{url('/')}}/public/website/images/arrow.svg" alt="">
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </section>
        <!-- // Tour Slider -->
        <!-- Our Philosophy -->
        <section class="ourPhilosophy commonSection parallax2">
            <h2 class="animatable fadeInDown">our<br> philosophy</h2>
            <div class="commonContent animatable fadeInUp">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus sodales nunc sed suscipit efficitur. Fusce quis est consectetur, mollis tortor vitae, tempus quam. Vestibulum tincidunt felis nec elit interdum, in convallis odio laoreet. Donec sit amet placerat augue. Praesent et scelerisque ipsum. Nulla libero ante, posuere non elit</p>
                <a href="#" class="animatable fadeInLeft">READ MORE <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 31.49 31.49" style="enable-background:new 0 0 31.49 31.49;" xml:space="preserve" width="512px" height="512px">
    <path d="M21.205,5.007c-0.429-0.444-1.143-0.444-1.587,0c-0.429,0.429-0.429,1.143,0,1.571l8.047,8.047H1.111  C0.492,14.626,0,15.118,0,15.737c0,0.619,0.492,1.127,1.111,1.127h26.554l-8.047,8.032c-0.429,0.444-0.429,1.159,0,1.587  c0.444,0.444,1.159,0.444,1.587,0l9.952-9.952c0.444-0.429,0.444-1.143,0-1.571L21.205,5.007z" />
</svg></a>
            </div>
        </section>
        <!-- // Our Philosophy -->
        <!-- Popular products -->
        <section class="popularProducts">
            <h2 class="animatable fadeInDown">Popular VEGETATION</h2>
            <div class="row">
                <div class="col-sm-3 animatable fadeInUp">
                    <figure style="background: url(images/popularPro1.jpg)">
                        <a href="JavaScript:void(0)" class="quickView" data-toggle="modal" data-target="#QuickViewModal">
                            <span>
                            <svg width="100" height="100" viewBox="0 0 30 30" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg">
                                <defs>
                                    <path d="M0,15.089434 C0,16.3335929 5.13666091,24.1788679 14.9348958,24.1788679 C24.7325019,24.1788679 29.8697917,16.3335929 29.8697917,15.089434 C29.8697917,13.8456167 24.7325019,6 14.9348958,6 C5.13666091,6 0,13.8456167 0,15.089434 Z" id="outline"></path>
                                    <mask id="mask">
                                        <rect width="100%" height="100%" fill="white"></rect>
                                        <use xlink:href="#outline" id="lid" fill="black" />
                                    </mask>
                                </defs>
                                <g id="eye">
                                    <path d="M0,15.089434 C0,16.3335929 5.13666091,24.1788679 14.9348958,24.1788679 C24.7325019,24.1788679 29.8697917,16.3335929 29.8697917,15.089434 C29.8697917,13.8456167 24.7325019,6 14.9348958,6 C5.13666091,6 0,13.8456167 0,15.089434 Z M14.9348958,22.081464 C11.2690863,22.081464 8.29688487,18.9510766 8.29688487,15.089434 C8.29688487,11.2277914 11.2690863,8.09740397 14.9348958,8.09740397 C18.6007053,8.09740397 21.5725924,11.2277914 21.5725924,15.089434 C21.5725924,18.9510766 18.6007053,22.081464 14.9348958,22.081464 L14.9348958,22.081464 Z M18.2535869,15.089434 C18.2535869,17.0200844 16.7673289,18.5857907 14.9348958,18.5857907 C13.1018339,18.5857907 11.6162048,17.0200844 11.6162048,15.089434 C11.6162048,13.1587835 13.1018339,11.593419 14.9348958,11.593419 C15.9253152,11.593419 14.3271242,14.3639878 14.9348958,15.089434 C15.451486,15.7055336 18.2535869,14.2027016 18.2535869,15.089434 L18.2535869,15.089434 Z" fill="#fff"></path>
                                    <use xlink:href="#outline" mask="url(#mask)" fill="#fff" />
                                </g>
                            </svg>
                            <br> QUICK VIEW
                            </span>
                        </a>
                    </figure>
                    <a href="product.html">
                        <h4>Melaleuca alternifolia</h4>
                        <h5>Bahera /Behada/Vibhitaka</h5>
                    </a>
                </div>
                <div class="col-sm-3 animatable fadeInUp">
                    <figure style="background: url(images/popularPro2.jpg)">
                        <a href="JavaScript:void(0)" class="quickView" data-toggle="modal" data-target="#QuickViewModal">
                            <span>
                            <svg width="100" height="100" viewBox="0 0 30 30" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg">
                                <defs>
                                    <path d="M0,15.089434 C0,16.3335929 5.13666091,24.1788679 14.9348958,24.1788679 C24.7325019,24.1788679 29.8697917,16.3335929 29.8697917,15.089434 C29.8697917,13.8456167 24.7325019,6 14.9348958,6 C5.13666091,6 0,13.8456167 0,15.089434 Z" id="outline"></path>
                                    <mask id="mask">
                                        <rect width="100%" height="100%" fill="white"></rect>
                                        <use xlink:href="#outline" id="lid" fill="black" />
                                    </mask>
                                </defs>
                                <g id="eye">
                                    <path d="M0,15.089434 C0,16.3335929 5.13666091,24.1788679 14.9348958,24.1788679 C24.7325019,24.1788679 29.8697917,16.3335929 29.8697917,15.089434 C29.8697917,13.8456167 24.7325019,6 14.9348958,6 C5.13666091,6 0,13.8456167 0,15.089434 Z M14.9348958,22.081464 C11.2690863,22.081464 8.29688487,18.9510766 8.29688487,15.089434 C8.29688487,11.2277914 11.2690863,8.09740397 14.9348958,8.09740397 C18.6007053,8.09740397 21.5725924,11.2277914 21.5725924,15.089434 C21.5725924,18.9510766 18.6007053,22.081464 14.9348958,22.081464 L14.9348958,22.081464 Z M18.2535869,15.089434 C18.2535869,17.0200844 16.7673289,18.5857907 14.9348958,18.5857907 C13.1018339,18.5857907 11.6162048,17.0200844 11.6162048,15.089434 C11.6162048,13.1587835 13.1018339,11.593419 14.9348958,11.593419 C15.9253152,11.593419 14.3271242,14.3639878 14.9348958,15.089434 C15.451486,15.7055336 18.2535869,14.2027016 18.2535869,15.089434 L18.2535869,15.089434 Z" fill="#fff"></path>
                                    <use xlink:href="#outline" mask="url(#mask)" fill="#fff" />
                                </g>
                            </svg>
                            <br> QUICK VIEW
                            </span>
                        </a>
                    </figure>
                    <a href="product.html">
                        <h4>Melaleuca alternifolia</h4>
                        <h5>Bahera /Behada/Vibhitaka</h5>
                    </a>
                </div>
                <div class="col-sm-3 animatable fadeInUp">
                    <figure style="background: url(images/popularPro3.jpg)">
                        <a href="JavaScript:void(0)" class="quickView" data-toggle="modal" data-target="#QuickViewModal">
                            <span>
                            <svg width="100" height="100" viewBox="0 0 30 30" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg">
                                <defs>
                                    <path d="M0,15.089434 C0,16.3335929 5.13666091,24.1788679 14.9348958,24.1788679 C24.7325019,24.1788679 29.8697917,16.3335929 29.8697917,15.089434 C29.8697917,13.8456167 24.7325019,6 14.9348958,6 C5.13666091,6 0,13.8456167 0,15.089434 Z" id="outline"></path>
                                    <mask id="mask">
                                        <rect width="100%" height="100%" fill="white"></rect>
                                        <use xlink:href="#outline" id="lid" fill="black" />
                                    </mask>
                                </defs>
                                <g id="eye">
                                    <path d="M0,15.089434 C0,16.3335929 5.13666091,24.1788679 14.9348958,24.1788679 C24.7325019,24.1788679 29.8697917,16.3335929 29.8697917,15.089434 C29.8697917,13.8456167 24.7325019,6 14.9348958,6 C5.13666091,6 0,13.8456167 0,15.089434 Z M14.9348958,22.081464 C11.2690863,22.081464 8.29688487,18.9510766 8.29688487,15.089434 C8.29688487,11.2277914 11.2690863,8.09740397 14.9348958,8.09740397 C18.6007053,8.09740397 21.5725924,11.2277914 21.5725924,15.089434 C21.5725924,18.9510766 18.6007053,22.081464 14.9348958,22.081464 L14.9348958,22.081464 Z M18.2535869,15.089434 C18.2535869,17.0200844 16.7673289,18.5857907 14.9348958,18.5857907 C13.1018339,18.5857907 11.6162048,17.0200844 11.6162048,15.089434 C11.6162048,13.1587835 13.1018339,11.593419 14.9348958,11.593419 C15.9253152,11.593419 14.3271242,14.3639878 14.9348958,15.089434 C15.451486,15.7055336 18.2535869,14.2027016 18.2535869,15.089434 L18.2535869,15.089434 Z" fill="#fff"></path>
                                    <use xlink:href="#outline" mask="url(#mask)" fill="#fff" />
                                </g>
                            </svg>
                            <br> QUICK VIEW
                            </span>
                        </a>
                    </figure>
                    <a href="product.html">
                        <h4>Melaleuca alternifolia</h4>
                        <h5>Bahera /Behada/Vibhitaka</h5>
                    </a>
                </div>
                <div class="col-sm-3 animatable fadeInUp">
                    <figure style="background: url(images/popularPro4.jpg)">
                        <a href="JavaScript:void(0)" class="quickView" data-toggle="modal" data-target="#QuickViewModal">
                            <span>
                            <svg width="100" height="100" viewBox="0 0 30 30" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg">
                                <defs>
                                    <path d="M0,15.089434 C0,16.3335929 5.13666091,24.1788679 14.9348958,24.1788679 C24.7325019,24.1788679 29.8697917,16.3335929 29.8697917,15.089434 C29.8697917,13.8456167 24.7325019,6 14.9348958,6 C5.13666091,6 0,13.8456167 0,15.089434 Z" id="outline"></path>
                                    <mask id="mask">
                                        <rect width="100%" height="100%" fill="white"></rect>
                                        <use xlink:href="#outline" id="lid" fill="black" />
                                    </mask>
                                </defs>
                                <g id="eye">
                                    <path d="M0,15.089434 C0,16.3335929 5.13666091,24.1788679 14.9348958,24.1788679 C24.7325019,24.1788679 29.8697917,16.3335929 29.8697917,15.089434 C29.8697917,13.8456167 24.7325019,6 14.9348958,6 C5.13666091,6 0,13.8456167 0,15.089434 Z M14.9348958,22.081464 C11.2690863,22.081464 8.29688487,18.9510766 8.29688487,15.089434 C8.29688487,11.2277914 11.2690863,8.09740397 14.9348958,8.09740397 C18.6007053,8.09740397 21.5725924,11.2277914 21.5725924,15.089434 C21.5725924,18.9510766 18.6007053,22.081464 14.9348958,22.081464 L14.9348958,22.081464 Z M18.2535869,15.089434 C18.2535869,17.0200844 16.7673289,18.5857907 14.9348958,18.5857907 C13.1018339,18.5857907 11.6162048,17.0200844 11.6162048,15.089434 C11.6162048,13.1587835 13.1018339,11.593419 14.9348958,11.593419 C15.9253152,11.593419 14.3271242,14.3639878 14.9348958,15.089434 C15.451486,15.7055336 18.2535869,14.2027016 18.2535869,15.089434 L18.2535869,15.089434 Z" fill="#fff"></path>
                                    <use xlink:href="#outline" mask="url(#mask)" fill="#fff" />
                                </g>
                            </svg>
                            <br> QUICK VIEW
                            </span>
                        </a>
                    </figure>
                    <a href="product.html">
                        <h4>Melaleuca alternifolia</h4>
                        <h5>Bahera /Behada/Vibhitaka</h5>
                    </a>
                </div>
            </div>
            <!-- Quick View Modal -->
            <div class="modal fade-scale" id="QuickViewModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog QuickView" role="document">
                    <div class="modal-content">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="389 332 31.112 31.112">
                                <path d="M31.112,1.414,29.7,0,15.556,14.142,1.414,0,0,1.414,14.142,15.556,0,29.7l1.414,1.414L15.556,16.97,29.7,31.112,31.112,29.7,16.97,15.556Z" transform="translate(389 332)" />
                            </svg>
                        </button>
                        <div class="modal-body">
                            <div class="row productContent">
                                <div class="col-sm-5 productImage">
                                    <img src="images/lakeViewTourImg1.jpg" alt="">
                                </div>
                                <div class="col-sm-7 productInfo">
                                    <h2>Ceratophyllum demersum L</h2>
                                    <div class="productDetails">
                                        <div class="row">
                                            <div class="col-sm-3">
                                                Family:
                                            </div>
                                            <div class="col-sm-9">
                                                Ceratophyllaceae
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                Synonyms:
                                            </div>
                                            <div class="col-sm-9">
                                                Ceratophyllum aquaticum H.C.Watson / Ceratophyllum cornutum Rich / Ceratophyllum indicum Willd. ex Cham / Dichotophyllum demersum (L.) Moench
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                Local Name:
                                            </div>
                                            <div class="col-sm-9">
                                                None
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                English Name:
                                            </div>
                                            <div class="col-sm-9">
                                                Rigid Hornwort , Coontail, Hornwort
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                Hindi name:
                                            </div>
                                            <div class="col-sm-9">
                                                kSoky (flokj)
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-5"></div>
                                <div class="col-sm-7">
                                    <a href="product.html" class="knowMore">KNOW MORE <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 31.49 31.49" style="enable-background:new 0 0 31.49 31.49;" xml:space="preserve" width="512px" height="512px">
    <path d="M21.205,5.007c-0.429-0.444-1.143-0.444-1.587,0c-0.429,0.429-0.429,1.143,0,1.571l8.047,8.047H1.111  C0.492,14.626,0,15.118,0,15.737c0,0.619,0.492,1.127,1.111,1.127h26.554l-8.047,8.032c-0.429,0.444-0.429,1.159,0,1.587  c0.444,0.444,1.159,0.444,1.587,0l9.952-9.952c0.444-0.429,0.444-1.143,0-1.571L21.205,5.007z" />
</svg></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- // Popular products -->
        <!-- Research Center -->
        <section class="researchCenter commonSection parallax3">
            <h2 class="animatable fadeInDown">Research<br>Center</h2>
            @foreach($researchcenters as $researchcenter)
            <div class="commonContent animatable fadeInUp">
                <p>{{$researchcenter->short_desc}}</p>
                <a href="#" class="animatable fadeInLeft">READ MORE <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 31.49 31.49" style="enable-background:new 0 0 31.49 31.49;" xml:space="preserve" width="512px" height="512px">
    <path d="M21.205,5.007c-0.429-0.444-1.143-0.444-1.587,0c-0.429,0.429-0.429,1.143,0,1.571l8.047,8.047H1.111  C0.492,14.626,0,15.118,0,15.737c0,0.619,0.492,1.127,1.111,1.127h26.554l-8.047,8.032c-0.429,0.444-0.429,1.159,0,1.587  c0.444,0.444,1.159,0.444,1.587,0l9.952-9.952c0.444-0.429,0.444-1.143,0-1.571L21.205,5.007z" />
</svg></a>
            </div>
            @endforeach
        </section>
        <!-- // Our Philosophy -->
        <!-- Latest News -->
        <section class="latestNews">
            <h2 class="animatable fadeInDown">latest News</h2>
            <div class="row">
                <div class="col-sm-4 animatable fadeInUp">
                    <figure style="background: url(images/latestNews1.jpg);"></figure>
                    <h4>LAUNCH</h4>
                    <h5>by Jane Doe  /  23.02.2016</h5>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus sodales nunc sed suscipit efficitur. Fusce quis est consec...</p>
                    <a href="#">READ MORE <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 31.49 31.49" style="enable-background:new 0 0 31.49 31.49;" xml:space="preserve" width="512px" height="512px">
    <path d="M21.205,5.007c-0.429-0.444-1.143-0.444-1.587,0c-0.429,0.429-0.429,1.143,0,1.571l8.047,8.047H1.111  C0.492,14.626,0,15.118,0,15.737c0,0.619,0.492,1.127,1.111,1.127h26.554l-8.047,8.032c-0.429,0.444-0.429,1.159,0,1.587  c0.444,0.444,1.159,0.444,1.587,0l9.952-9.952c0.444-0.429,0.444-1.143,0-1.571L21.205,5.007z" />
</svg></a>
                </div>
                <div class="col-sm-4 animatable fadeInUp">
                    <figure style="background: url(images/latestNews2.jpg);"></figure>
                    <h4>Research center</h4>
                    <h5>by Jane Doe  /  23.02.2016</h5>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus sodales nunc sed suscipit efficitur. Fusce quis est consec...</p>
                    <a href="#">READ MORE <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 31.49 31.49" style="enable-background:new 0 0 31.49 31.49;" xml:space="preserve" width="512px" height="512px">
    <path d="M21.205,5.007c-0.429-0.444-1.143-0.444-1.587,0c-0.429,0.429-0.429,1.143,0,1.571l8.047,8.047H1.111  C0.492,14.626,0,15.118,0,15.737c0,0.619,0.492,1.127,1.111,1.127h26.554l-8.047,8.032c-0.429,0.444-0.429,1.159,0,1.587  c0.444,0.444,1.159,0.444,1.587,0l9.952-9.952c0.444-0.429,0.444-1.143,0-1.571L21.205,5.007z" />
</svg></a>
                </div>
                <div class="col-sm-4 animatable fadeInUp">
                    <figure style="background: url(images/latestNews3.jpg);"></figure>
                    <h4>NEW HERB</h4>
                    <h5>by Jane Doe  /  23.02.2016</h5>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus sodales nunc sed suscipit efficitur. Fusce quis est consec...</p>
                    <a href="#">READ MORE <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 31.49 31.49" style="enable-background:new 0 0 31.49 31.49;" xml:space="preserve" width="512px" height="512px">
    <path d="M21.205,5.007c-0.429-0.444-1.143-0.444-1.587,0c-0.429,0.429-0.429,1.143,0,1.571l8.047,8.047H1.111  C0.492,14.626,0,15.118,0,15.737c0,0.619,0.492,1.127,1.111,1.127h26.554l-8.047,8.032c-0.429,0.444-0.429,1.159,0,1.587  c0.444,0.444,1.159,0.444,1.587,0l9.952-9.952c0.444-0.429,0.444-1.143,0-1.571L21.205,5.007z" />
</svg></a>
                </div>
            </div>
        </section>
        <!-- // Latest News -->
        <!-- footer -->
        
        <!-- // footer -->
        <!-- Watch Video -->
        <div class="watchVideoCont">
            <a href="JavaScript:void(0)" class="popClose">&times;</a>
            <iframe src="https://www.youtube.com/embed/dIulcjcs_tw?rel=0&amp;showinfo=0?ecver=2" width="640" height="360" frameborder="0" style="position:absolute;width:100%;height:100%;left:0" allowfullscreen></iframe>
        </div>
    </div>
@endsection