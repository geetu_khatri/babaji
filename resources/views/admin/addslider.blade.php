@extends('layouts.adminpage')
@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
      Add Slider
     
      </h1>
   
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title successfull" style="display: none">Slider Added Successfully!</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" id="slider_f" name="slider_f">
              <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Slider Caption</label>
                  <input type="text" class="form-control" id="caption" placeholder="Enter Caption" name="caption">
                </div>
                <div class="form-group inp1">
                <input type="hidden" name="filename" class="file_name" id="filename">
                
                  <label for="exampleInputFile">File input</label>
                  <input type="file" id="slider_image" name="slider_image" onchange="uploadimage('slider_image');">

                  <p class="help-block">Image Size:1366*670</p>
                </div>
                <input type="hidden" name="_token" value="{{ csrf_token()}}">
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.box -->

        

        </div>
        <!--/.col (left) -->
        <!-- right column -->
      
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
@endsection
