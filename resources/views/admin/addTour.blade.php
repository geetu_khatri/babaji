@extends('layouts.adminpage')
@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
      Add Tour Details
     
      </h1>
   
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title successfull" style="display: none">Tour Added Successfully!</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" id="tour_f" name="tour_f">
              <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Title</label>
                  <input type="text" class="form-control" id="title" placeholder="Enter Title" name="title">
                </div>
                <div class="form-group inp1">
                   <input type="hidden" name="filename" class="file_name" id="filename">
                
                  <label for="exampleInputFile">File input</label>
                  <input type="file" id="tour_image" name="tour_image" onchange="uploadimage('tour_image');">

                  <p class="help-block">Image Size:1366*670</p>
                </div>
                   <div class="form-group">
                                <label for="exampleInputEmail1">Description</label>
                                <textarea class="form-control" id="description" name="description"></textarea>

                            </div>
                     <div class="form-group">
                  <label for="exampleInputEmail1">Time</label>
                  <input type="text" class="form-control" id="time" placeholder="Enter Time" name="time">
                </div>
                <input type="hidden" name="_token" value="{{ csrf_token()}}">
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.box -->

        

        </div>
        <!--/.col (left) -->
        <!-- right column -->
      
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
@endsection
