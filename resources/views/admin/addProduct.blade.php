@extends('layouts.adminpage')
@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Add Product

        </h1>

    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title successfull" style="display: none">Product Added Successfully!</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form role="form" id="product_f" name="product_f">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Product Name</label>
                                <input type="text" class="form-control" id="product_name" placeholder="Enter Product Name" name="product_name">
                            </div>
                            <div class="form-group">
                                  <input type="hidden" name="filename" class="file_name" id="filename">
                
                                <label for="exampleInputFile">Product Image</label>
                                <input type="file" id="product_image" name="product_image" onchange="uploadimage('product_image');">

                                <p class="help-block">Image Size:560*260</p>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Family</label>
                                <input type="text" class="form-control" id="family_name" placeholder="Enter Family Name" name="family_name">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Synonyms</label>
                                <textarea class="form-control" id="synonyms" name="synonyms"></textarea>

                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Local Name</label>
                                <input type="text" class="form-control" id="local_name" placeholder="Enter Local Name" name="local_name">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">English Name</label>
                                <input type="text" class="form-control" id="english_name" placeholder="Enter English  Name" name="english_name">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Hindi Name</label>
                                <input type="text" class="form-control" id="hindi_name" placeholder="Enter Hindi  Name" name="hindi_name">
                            </div>
                             <div class="form-group">
                                <label for="exampleInputEmail1">Medical Use</label>
                                <textarea class="form-control" id="medical_use" name="medical_use"></textarea>

                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Brief Description</label>
                                <textarea class="form-control" id="brief_desc" name="brief_desc"></textarea>

                            </div>
                             <div class="form-group">
                                <label for="exampleInputEmail1">Distribution</label>
                                <textarea class="form-control" id="distribution" name="distribution"></textarea>
                               
                            </div>
                            
                            <input type="hidden" name="_token" value="{{ csrf_token()}}">
                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
                <!-- /.box -->



            </div>
            <!--/.col (left) -->
            <!-- right column -->

            <!--/.col (right) -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
@endsection
