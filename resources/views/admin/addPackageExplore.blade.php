@extends('layouts.adminpage')
@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
      Add Package Details
     
      </h1>
   
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title successfull" style="display: none">Package Added Successfully!</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" id="package_f" name="package_f">
              <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Tour Name</label>
                  <input type="text" class="form-control" id="tour_name" placeholder="Enter Title" name="tour_name">
                </div>
                   <div class="form-group">
                  <label for="exampleInputEmail1">Caption</label>
                  <input type="text" class="form-control" id="caption" placeholder="Caption" name="caption">
                </div>
                <div class="form-group inp1">
                   
                  <label for="exampleInputFile">Image</label>
                  <input type="hidden" name="tourimagename" class="file_name" id="tourimagename">
                  <input type="file" id="tour_image" name="tour_image" onchange="uploadimage('tour_image');">
                  
                  <p class="help-block">Image Size:</p>
                </div>
                 
   <div class="form-group inp1">
                     <input type="hidden" name="threedimage" class="file_name" id="threedimage">
                  <label for="exampleInputFile">3 D Image</label>
                  <input type="file" id="threed_image" name="threed_image" onchange="uploadimage('threed_image');">

                  <p class="help-block">Image Size:</p>
                </div>
                           <div class="form-group">
                  <label for="exampleInputEmail1">Select Main Tour</label>
                  <select class="form-control" name="main_tour" id="main_tour">
                     @foreach($tours as $tour)
                     
                      <option value="{{$tour->id}}">{{$tour->title}}</option>
                     @endforeach 
                  </select>
                  
                </div>            
                     
                <input type="hidden" name="_token" value="{{ csrf_token()}}">
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.box -->

        

        </div>
        <!--/.col (left) -->
        <!-- right column -->
      
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
@endsection
