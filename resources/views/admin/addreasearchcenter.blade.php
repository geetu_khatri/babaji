@extends('layouts.adminpage')
@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
      Add Research Center
     
      </h1>
   
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title successfull" style="display: none">Research Center Data Added Successfully!</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" id="research_f" name="research_f">
              <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Short description</label>
                  <textarea name="short_desc" id="short_desc" class="form-control" ></textarea>
            
                </div>
                <div class="form-group">
                    <input type="hidden" name="filename" class="filename">
                  <label for="exampleInputFile">Full Description</label>
                   <textarea name="full_desc" id="full_desc" class="form-control" ></textarea>
              
                </div>
                <input type="hidden" name="_token" value="{{ csrf_token()}}">
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.box -->

        

        </div>
        <!--/.col (left) -->
        <!-- right column -->
      
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
@endsection
