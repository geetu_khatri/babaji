@extends('layouts.adminpage')
@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
      Add Category
     
      </h1>
   
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title successfull" style="display: none">Category Added Successfully!</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" id="catgory_f" name="catgory_f">
              <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Category Name</label>
                  <input type="text" class="form-control" id="category_name" placeholder="Enter Category Name" name="category_name">
                </div>
                <div class="form-group inp1">
                     <input type="hidden" name="filename" class="file_name" id="filename">
                
                  <label for="exampleInputFile">Category Image</label>
                  <input type="file" id="catgeory_image" name="catgeory_image" onchange="uploadimage('catgeory_image');">

                  <p class="help-block">Image Size:560*260</p>
                </div>
                <input type="hidden" name="_token" value="{{ csrf_token()}}">
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.box -->

        

        </div>
        <!--/.col (left) -->
        <!-- right column -->
      
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
@endsection
