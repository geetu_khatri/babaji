<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{asset('website/images/whfLogo.png')}}" class="img-circle" alt="User Image">
        </div>
       
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        <li class="active treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span> <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li class="active"><a href="index.html"><i class="fa fa-circle-o"></i> Dashboard v1</a></li>
            <li><a href="index2.html"><i class="fa fa-circle-o"></i> Dashboard v2</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Slider</span><i class="fa fa-angle-left pull-right"></i>
        
          </a>
          <ul class="treeview-menu">
            <li><a href="{{url('/')}}/addslider"><i class="fa fa-circle-o"></i> Add Slider</a></li>
            
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Research Center</span><i class="fa fa-angle-left pull-right"></i>
        
          </a>
          <ul class="treeview-menu">
            <li><a href="{{url('/')}}/researchcenter"><i class="fa fa-circle-o"></i>Add Research Center</a></li>
            
          </ul>
        </li>
          <li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Products</span><i class="fa fa-angle-left pull-right"></i>
        
          </a>
          <ul class="treeview-menu">
            <li><a href="{{url('/')}}/addProduct"><i class="fa fa-circle-o"></i>Add Product</a></li>
             <li><a href="{{url('/')}}/addcategory"><i class="fa fa-circle-o"></i>Add Category</a></li>
            
          </ul>
        </li>
            <li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Tour</span><i class="fa fa-angle-left pull-right"></i>
        
          </a>
          <ul class="treeview-menu">
            <li><a href="{{url('/')}}/addTour"><i class="fa fa-circle-o"></i>Add tour</a></li>
             <li><a href="{{url('/')}}/addPackage"><i class="fa fa-circle-o"></i>Tour Package Explore</a></li>
            
          </ul>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>