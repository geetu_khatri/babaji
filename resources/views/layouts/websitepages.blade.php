<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Patanjali</title>
    <!-- Bootstrap -->
    <link href="{{asset('website/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Oswald:400,500|Roboto:300,400,500,700" rel="stylesheet">
    <link href="{{asset('website/css/style.css?ver=1')}}" rel="stylesheet">
    <link href="{{asset('website/css/responsive.css?ver=1')}}" rel="stylesheet">
    <link href="{{asset('website/css/animate.min.css?ver=1')}}" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
     @yield('content')
     <footer class="footer">
        <div class="row fLinks">
            <div class="col-sm-4">
                <h4 class="animatable fadeInDown">Opening Hours</h4>
                <ul class="list-unstyled timming">
                    <li class="animatable fadeInUp"><span>tuesday</span> <span>09:00 - 17:00</span></li>
                    <li class="animatable fadeInUp"><span>Wednesday</span> <span>09:00 - 17:00</span></li>
                    <li class="animatable fadeInUp"><span>Thursday</span> <span>09:00 - 17:00</span></li>
                    <li class="animatable fadeInUp"><span>Friday</span> <span>09:00 - 17:00</span></li>
                    <li class="animatable fadeInUp"><span>Saturday</span> <span>09:00 - 17:00</span></li>
                    <li class="animatable fadeInUp"><span>Sunday</span> <span>09:00 - 17:00</span></li>
                    <li class="animatable fadeInUp"><span>Monday</span> <span>Closed</span></li>
                </ul>
            </div>
            <div class="col-sm-1">
            </div>
            <div class="col-sm-3">
                <h4 class="animatable fadeInDown">navigation</h4>
                <ul class="list-unstyled">
                    <li class="animatable fadeInUp"><a href="#">Experience</a></li>
                    <li class="animatable fadeInUp"><a href="#">Products</a></li>
                    <li class="animatable fadeInUp"><a href="#">Forest Safari</a></li>
                    <li class="animatable fadeInUp"><a href="#">Research Center</a></li>
                    <li class="animatable fadeInUp"><a href="#">News</a></li>
                    <li class="animatable fadeInUp"><a href="#">About Us</a></li>
                    <li class="animatable fadeInUp"><a href="#">Contact</a></li>
                </ul>
            </div>
            <div class="col-sm-4">
                <h4 class="animatable fadeInDown">Contact us</h4>
                <p class="animatable fadeInUp">World Herbal Center
                    <br> Morni, Morni hills-160017
                    <br> Phone : 0172-2702955-56
                    <br> Email : <a href="mailto:worldherbalcenter@gmail.com">worldherbalcenter@gmail.com</a> </p>
            </div>
        </div>
        <div class="row social">
            <ul class="list-unstyled">
                <li class="animatable fadeInUp">
                    <a href="#">
                        <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="430.113px" height="430.114px" viewBox="0 0 430.113 430.114" style="enable-background:new 0 0 430.113 430.114;" xml:space="preserve">
                            <g>
                                <path id="Facebook" d="M158.081,83.3c0,10.839,0,59.218,0,59.218h-43.385v72.412h43.385v215.183h89.122V214.936h59.805
    c0,0,5.601-34.721,8.316-72.685c-7.784,0-67.784,0-67.784,0s0-42.127,0-49.511c0-7.4,9.717-17.354,19.321-17.354
    c9.586,0,29.818,0,48.557,0c0-9.859,0-43.924,0-75.385c-25.016,0-53.476,0-66.021,0C155.878-0.004,158.081,72.48,158.081,83.3z" />
                            </g>
                        </svg>
                    </a>
                </li>
                <li class="animatable fadeInUp">
                    <a href="#">
                        <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 612 612" style="enable-background:new 0 0 612 612;" xml:space="preserve">
                            <g>
                                <path d="M612,116.258c-22.525,9.981-46.694,16.75-72.088,19.772c25.929-15.527,45.777-40.155,55.184-69.411
      c-24.322,14.379-51.169,24.82-79.775,30.48c-22.907-24.437-55.49-39.658-91.63-39.658c-69.334,0-125.551,56.217-125.551,125.513
      c0,9.828,1.109,19.427,3.251,28.606C197.065,206.32,104.556,156.337,42.641,80.386c-10.823,18.51-16.98,40.078-16.98,63.101
      c0,43.559,22.181,81.993,55.835,104.479c-20.575-0.688-39.926-6.348-56.867-15.756v1.568c0,60.806,43.291,111.554,100.693,123.104
      c-10.517,2.83-21.607,4.398-33.08,4.398c-8.107,0-15.947-0.803-23.634-2.333c15.985,49.907,62.336,86.199,117.253,87.194
      c-42.947,33.654-97.099,53.655-155.916,53.655c-10.134,0-20.116-0.612-29.944-1.721c55.567,35.681,121.536,56.485,192.438,56.485
      c230.948,0,357.188-191.291,357.188-357.188l-0.421-16.253C573.872,163.526,595.211,141.422,612,116.258z" />
                            </g>
                        </svg>
                    </a>
                </li>
                <li class="animatable fadeInUp">
                    <a href="#">
                        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="169.063px" height="169.063px" viewBox="0 0 169.063 169.063" style="enable-background:new 0 0 169.063 169.063;" xml:space="preserve">
                            <g>
                                <path d="M122.406,0H46.654C20.929,0,0,20.93,0,46.655v75.752c0,25.726,20.929,46.655,46.654,46.655h75.752
    c25.727,0,46.656-20.93,46.656-46.655V46.655C169.063,20.93,148.133,0,122.406,0z M154.063,122.407
    c0,17.455-14.201,31.655-31.656,31.655H46.654C29.2,154.063,15,139.862,15,122.407V46.655C15,29.201,29.2,15,46.654,15h75.752
    c17.455,0,31.656,14.201,31.656,31.655V122.407z" />
                                <path d="M84.531,40.97c-24.021,0-43.563,19.542-43.563,43.563c0,24.02,19.542,43.561,43.563,43.561s43.563-19.541,43.563-43.561
    C128.094,60.512,108.552,40.97,84.531,40.97z M84.531,113.093c-15.749,0-28.563-12.812-28.563-28.561
    c0-15.75,12.813-28.563,28.563-28.563s28.563,12.813,28.563,28.563C113.094,100.281,100.28,113.093,84.531,113.093z" />
                                <path d="M129.921,28.251c-2.89,0-5.729,1.17-7.77,3.22c-2.051,2.04-3.23,4.88-3.23,7.78c0,2.891,1.18,5.73,3.23,7.78
    c2.04,2.04,4.88,3.22,7.77,3.22c2.9,0,5.73-1.18,7.78-3.22c2.05-2.05,3.22-4.89,3.22-7.78c0-2.9-1.17-5.74-3.22-7.78
    C135.661,29.421,132.821,28.251,129.921,28.251z" />
                            </g>
                        </svg>
                    </a>
                </li>
                <li class="animatable fadeInUp">
                    <a href="#">
                        <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="90px" height="90px" viewBox="0 0 90 90" style="enable-background:new 0 0 90 90;" xml:space="preserve">
                            <g>
                                <path id="YouTube" d="M70.939,65.832H66l0.023-2.869c0-1.275,1.047-2.318,2.326-2.318h0.315c1.282,0,2.332,1.043,2.332,2.318
    L70.939,65.832z M52.413,59.684c-1.253,0-2.278,0.842-2.278,1.873V75.51c0,1.029,1.025,1.869,2.278,1.869
    c1.258,0,2.284-0.84,2.284-1.869V61.557C54.697,60.525,53.671,59.684,52.413,59.684z M82.5,51.879v26.544
    C82.5,84.79,76.979,90,70.23,90H19.771C13.02,90,7.5,84.79,7.5,78.423V51.879c0-6.367,5.52-11.578,12.271-11.578H70.23
    C76.979,40.301,82.5,45.512,82.5,51.879z M23.137,81.305l-0.004-27.961l6.255,0.002v-4.143l-16.674-0.025v4.073l5.205,0.015v28.039
    H23.137z M41.887,57.509h-5.215v14.931c0,2.16,0.131,3.24-0.008,3.621c-0.424,1.158-2.33,2.388-3.073,0.125
    c-0.126-0.396-0.015-1.591-0.017-3.643l-0.021-15.034h-5.186l0.016,14.798c0.004,2.268-0.051,3.959,0.018,4.729
    c0.127,1.357,0.082,2.939,1.341,3.843c2.346,1.69,6.843-0.252,7.968-2.668l-0.01,3.083l4.188,0.005L41.887,57.509L41.887,57.509z
     M58.57,74.607L58.559,62.18c-0.004-4.736-3.547-7.572-8.356-3.74l0.021-9.239l-5.209,0.008l-0.025,31.89l4.284-0.062l0.39-1.986
    C55.137,84.072,58.578,80.631,58.57,74.607z M74.891,72.96l-3.91,0.021c-0.002,0.155-0.008,0.334-0.01,0.529v2.182
    c0,1.168-0.965,2.119-2.137,2.119h-0.766c-1.174,0-2.139-0.951-2.139-2.119V75.45v-2.4v-3.097h8.954v-3.37
    c0-2.463-0.063-4.925-0.267-6.333c-0.641-4.454-6.893-5.161-10.051-2.881c-0.991,0.712-1.748,1.665-2.188,2.945
    c-0.444,1.281-0.665,3.031-0.665,5.254v7.41C61.714,85.296,76.676,83.555,74.891,72.96z M54.833,32.732
    c0.269,0.654,0.687,1.184,1.254,1.584c0.56,0.394,1.276,0.592,2.134,0.592c0.752,0,1.418-0.203,1.998-0.622
    c0.578-0.417,1.065-1.04,1.463-1.871l-0.099,2.046h5.813V9.74H62.82v19.24c0,1.042-0.858,1.895-1.907,1.895
    c-1.043,0-1.904-0.853-1.904-1.895V9.74h-4.776v16.674c0,2.124,0.039,3.54,0.102,4.258C54.4,31.385,54.564,32.069,54.833,32.732z
     M37.217,18.77c0-2.373,0.198-4.226,0.591-5.562c0.396-1.331,1.107-2.401,2.137-3.208c1.027-0.811,2.342-1.217,3.941-1.217
    c1.345,0,2.497,0.264,3.459,0.781c0.967,0.52,1.713,1.195,2.23,2.028c0.527,0.836,0.885,1.695,1.076,2.574
    c0.195,0.891,0.291,2.235,0.291,4.048v6.252c0,2.293-0.092,3.98-0.271,5.051c-0.177,1.074-0.557,2.07-1.146,3.004
    c-0.58,0.924-1.329,1.615-2.237,2.056c-0.918,0.445-1.968,0.663-3.154,0.663c-1.325,0-2.441-0.183-3.361-0.565
    c-0.923-0.38-1.636-0.953-2.144-1.714c-0.513-0.762-0.874-1.69-1.092-2.772c-0.219-1.081-0.323-2.707-0.323-4.874L37.217,18.77
    L37.217,18.77z M41.77,28.59c0,1.4,1.042,2.543,2.311,2.543c1.27,0,2.308-1.143,2.308-2.543V15.43c0-1.398-1.038-2.541-2.308-2.541
    c-1.269,0-2.311,1.143-2.311,2.541V28.59z M25.682,35.235h5.484l0.006-18.96l6.48-16.242h-5.998l-3.445,12.064L24.715,0h-5.936
    l6.894,16.284L25.682,35.235z" />
                            </g>
                        </svg>
                    </a>
                </li>
            </ul>
        </div>
    </footer>
    <!-- // footer -->
 <!-- Quick View Modal -->
    <div class="modal fade-scale" id="QuickViewModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog QuickView" role="document">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="389 332 31.112 31.112">
                        <path d="M31.112,1.414,29.7,0,15.556,14.142,1.414,0,0,1.414,14.142,15.556,0,29.7l1.414,1.414L15.556,16.97,29.7,31.112,31.112,29.7,16.97,15.556Z" transform="translate(389 332)" />
                    </svg>
                </button>
                <div class="modal-body">
                    <div class="row productContent">
                        <div class="col-sm-5 productImage">
                            <img src="images/lakeViewTourImg1.jpg" alt="">
                        </div>
                        <div class="col-sm-7 productInfo">
                            <h2>Ceratophyllum demersum L</h2>
                            <div class="productDetails">
                                <div class="row">
                                    <div class="col-sm-3">
                                        Family:
                                    </div>
                                    <div class="col-sm-9">
                                        Ceratophyllaceae
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-3">
                                        Synonyms:
                                    </div>
                                    <div class="col-sm-9">
                                        Ceratophyllum aquaticum H.C.Watson / Ceratophyllum cornutum Rich / Ceratophyllum indicum Willd. ex Cham / Dichotophyllum demersum (L.) Moench
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-3">
                                        Local Name:
                                    </div>
                                    <div class="col-sm-9">
                                        None
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-3">
                                        English Name:
                                    </div>
                                    <div class="col-sm-9">
                                        Rigid Hornwort , Coontail, Hornwort
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-3">
                                        Hindi name:
                                    </div>
                                    <div class="col-sm-9">
                                        kSoky (flokj)
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-5"></div>
                        <div class="col-sm-7">
                            <a href="product.html" class="knowMore">KNOW MORE <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 31.49 31.49" style="enable-background:new 0 0 31.49 31.49;" xml:space="preserve" width="512px" height="512px">
    <path d="M21.205,5.007c-0.429-0.444-1.143-0.444-1.587,0c-0.429,0.429-0.429,1.143,0,1.571l8.047,8.047H1.111  C0.492,14.626,0,15.118,0,15.737c0,0.619,0.492,1.127,1.111,1.127h26.554l-8.047,8.032c-0.429,0.444-0.429,1.159,0,1.587  c0.444,0.444,1.159,0.444,1.587,0l9.952-9.952c0.444-0.429,0.444-1.143,0-1.571L21.205,5.007z" />
</svg></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="{{asset('website/js/jquery.min.js')}}"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="{{asset('website/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('website/js/jquery.vide.js?ver=1')}}"></script>
    <script src="{{asset('website/js/custom.js?ver=1')}}"></script>
    <script type="text/javascript">
    $(window).scroll(function() {
        var x = $(this).scrollTop();
        $('.parallax1').css('background-position', '50%' + parseInt(-x / 3) + 'px');
        var offsetParallax2 = $('.parallax2').offset().top - 500;
        if (x > offsetParallax2) {
            $('.parallax2').css('background-position', '50%' + parseInt((x - offsetParallax2 - 500)) + '%');
        }

        var offsetParallax3 = $('.parallax3').offset().top - 500;
        if (x > offsetParallax3) {
            $('.parallax3').css('background-position', '50%' + parseInt((x - offsetParallax3 - 500)) + '%');
        }
    });
    </script>
</body>

</html>
