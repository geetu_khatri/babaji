@extends('layouts.websitepages')
@section('content')
    <!-- THF Logo -->
    <a href="index.html" class="whfTrans animatable fadeInLeft"><img src="images/whfTransprent.png" alt="WORLD HERBAL FOREST"></a>
    <!-- back to top -->
    <a href="index.html" class="backToMap">
        <svg id="Capa_1" data-name="Capa 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1080 1080">
            <path class="cls-1" d="M960,0C661.78,0,420,241.78,420,540s241.78,540,540,540,540-241.7,540-540S1258.29,0,960,0Zm0,1008c-258,0-468-210-468-468S702,72,960,72s468,210,468,468S1218,1008,960,1008Z" transform="translate(-420)" />
            <path class="cls-1" d="M992.19,280.94a36,36,0,0,0-50.9,0l-233.5,233.5a36.33,36.33,0,0,0,0,51.12l233.49,233.5a36,36,0,0,0,50.9-50.9L784,540,992.19,331.85A36,36,0,0,0,992.19,280.94Z" transform="translate(-420)" />
            <path class="cls-1" d="M1212.19,280.94a36,36,0,0,0-50.9,0l-233.5,233.5a36.33,36.33,0,0,0,0,51.12l233.49,233.5a36,36,0,0,0,50.9-50.9L1004,540l208.15-208.15A36,36,0,0,0,1212.19,280.94Z" transform="translate(-420)" />
        </svg> BACK TO MAP
    </a>
    <!-- Social 360 -->
    <div class="social360">
        <div class="share">
            <span>
            <a href="#">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="-135.21 -192.01 1080 1080">
                    <path d="M-642.79-275c-298.23,0-540,241.77-540,540s241.77,540,540,540,540-241.77,540-540S-344.55-275-642.79-275ZM-493.95,55.37h-78.46c-12.87,0-31,6.43-31,33.82v73h109.1L-507,286.15h-96.36v353h-146v-353h-69.43V162.09h69.43V81.81c0-57.41,27.27-147.31,147.29-147.31l108.14.45Z" transform="translate(1047.58 82.99)" />
                </svg>
            </a>
            <a href="#">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="380 323 49.66 49.66">
                    <path d="M192.83-38A24.86,24.86,0,0,0,168-13.17a24.86,24.86,0,0,0,24.83,24.83,24.86,24.86,0,0,0,24.83-24.83A24.85,24.85,0,0,0,192.83-38ZM203.9-18.86v.74c0,7.55-5.75,16.26-16.26,16.26a16.16,16.16,0,0,1-8.76-2.57,11.63,11.63,0,0,0,1.36.08,11.47,11.47,0,0,0,7.1-2.45,5.72,5.72,0,0,1-5.34-4,5.72,5.72,0,0,0,1.07.1,5.72,5.72,0,0,0,1.51-.2A5.72,5.72,0,0,1,180-16.5v-.07a5.69,5.69,0,0,0,2.59.71,5.71,5.71,0,0,1-2.54-4.75,5.69,5.69,0,0,1,.77-2.87,16.23,16.23,0,0,0,11.78,6,5.73,5.73,0,0,1-.15-1.3,5.72,5.72,0,0,1,5.71-5.71,5.71,5.71,0,0,1,4.17,1.8A11.4,11.4,0,0,0,206-24.07a5.74,5.74,0,0,1-2.52,3.16,11.35,11.35,0,0,0,3.28-.9,11.509,11.509,0,0,1-2.86,2.95Z" transform="translate(212 361)" />
                </svg>
            </a>
            </span>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="170 111 469.919 473.932">
                <g transform="translate(167.994 111)">
                    <path class="a" d="M385.513,301.214c-27.438,0-51.64,13.072-67.452,33.09l-146.66-75a85.8,85.8,0,0,0,3.3-22.347,85.09,85.09,0,0,0-3.926-24.224l146.013-74.656a85.98,85.98,0,1,0-17.579-51.749,85.421,85.421,0,0,0,3.322,22.412l-146.639,75c-15.833-20.039-40.079-33.154-67.56-33.154a86.359,86.359,0,1,0,68.832,138.053L303.112,363.3a85.3,85.3,0,0,0-3.947,24.289,86.358,86.358,0,1,0,86.348-86.37Z" />
                </g>
            </svg>
        </div>
        <div id="bars">
            <div class="bar"></div>
            <div class="bar"></div>
            <div class="bar"></div>
            <div class="bar"></div>
            <div class="bar"></div>
            <div class="bar"></div>
            <div class="bar"></div>
        </div>
    </div>
    <!-- Click and drag -->
    <div class="clickAndDragContainer">
        <div class="clickAndDrag">
            <div class="dragIcon">
                <figure>
                    <svg id="Capa_1" data-name="Capa 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 914.69 1080">
                        <path d="M1308.29,333.32a107.82,107.82,0,0,0-43,8.91c-16-40.55-55.41-69.3-101.34-69.3a107.8,107.8,0,0,0-47.61,11c-18.21-35.12-54.72-59.16-96.72-59.16a107.72,107.72,0,0,0-35.28,5.93V109.82C984.34,49.27,935.41,0,875.27,0S766.2,49.27,766.2,109.82V544.89L687.65,455c-.55-.62-1.11-1.22-1.69-1.8A109.35,109.35,0,0,0,608.67,421h-.56a107.88,107.88,0,0,0-76.89,31.72c-36.07,36.08-38.06,89.63-5.46,146.94C568,673.8,613.83,743.72,654.3,805.41c29.58,45.08,57.52,87.67,77.95,122.87C750,958.83,797,1057.92,797.46,1058.92A36.88,36.88,0,0,0,830.78,1080h466.44a36.9,36.9,0,0,0,35.12-25.58c8.7-27.05,85-266.9,85-359.53V443.15C1417.35,382.58,1368.43,333.32,1308.29,333.32ZM1273,443.15c0-19.88,15.83-36.06,35.27-36.06s35.28,16.17,35.28,36.06V694.89c0,61.56-47.31,227-73.41,311.34H854c-15.29-31.83-44-90.78-58-115-21.44-37-49.93-80.38-80.08-126.34-39.88-60.76-85.07-129.64-126.11-201.75-10.71-18.84-20.53-44.24-6.47-58.3a35.21,35.21,0,0,1,24.91-10.11,36.05,36.05,0,0,1,24.75,9.89L775.32,667.41A36.89,36.89,0,0,0,840,643.13V109.82c0-19.88,15.83-36,35.29-36s35.29,16.17,35.29,36v344.4a36.89,36.89,0,0,0,73.77,0V334.6c0-19.87,15.83-36,35.28-36s35.28,16.16,35.28,36V502.34a36.89,36.89,0,1,0,73.77,0V382.75c0-19.88,15.83-36.05,35.28-36.05s35.29,16.17,35.29,36.05V550.49a36.89,36.89,0,1,0,73.78,0V443.15Z" transform="translate(-502.66)" />
                    </svg>
                    <span class="vertical"></span>
                    <span class="horizontal"></span>
                </figure>
            </div>
            <h4>Click and drag to navigate</h4>
            <a href="#">OK</a>
        </div>
    </div>
    <!-- More View Slide -->
    <div class="moreViewSlide">
        <a href="JavaScript:void(0)" class="slideToggle"><img src="images/dragArrow.svg"></a>
        <ul class="list-unstyled">
            <li class="moreView">
                <div class="figureCont">
                    <figure style="background: url(images/moreView1.jpg);">
                    </figure>
                </div>
                <h4>Tikkar lake</h4>
            </li>
            <li class="moreView active">
                <div class="figureCont">
                    <figure style="background: url(images/moreView1.jpg);">
                    </figure>
                </div>
                <h4>Tikkar lake</h4>
            </li>
            <li class="moreView">
                <div class="figureCont">
                    <figure style="background: url(images/moreView1.jpg);">
                    </figure>
                </div>
                <h4>Tikkar lake</h4>
            </li>
        </ul>
    </div>
    <!-- Other Areas -->
    <a href="#" class="otherAreaClick active animatable bounceIn" id="otherAreaClick">
        <svg id="viewOther" xmlns="http://www.w3.org/2000/svg" viewBox="234 171 79 79">
            <rect width="33" height="33" transform="translate(234 171)" />
            <rect width="33" height="33" transform="translate(280 171)" />
            <rect width="33" height="33" transform="translate(234 217)" />
            <rect width="33" height="33" transform="translate(280 217)" />
        </svg>
        <svg id="view360" xmlns="http://www.w3.org/2000/svg" viewBox="148 156 512.001 383.16">
            <g transform="translate(148 91.58)">
                <path d="M381.747,210.816a57.138,57.138,0,1,0,57.138,57.138A57.2,57.2,0,0,0,381.747,210.816Z" />
                <path d="M130.253,210.816a57.138,57.138,0,1,0,57.138,57.138A57.2,57.2,0,0,0,130.253,210.816Z" />
                <path d="M495.2,136.123H461.719v-54.9a16.805,16.805,0,0,0-16.8-16.805H67.087A16.805,16.805,0,0,0,50.282,81.225v54.9H16.805A16.807,16.807,0,0,0,0,152.928V430.775a16.805,16.805,0,0,0,16.805,16.8H203.828A16.808,16.808,0,0,0,220.4,433.546l8.059-48.192a26.231,26.231,0,0,1,25.951-21.972h3.173a26.232,26.232,0,0,1,25.951,21.972l8.059,48.192a16.807,16.807,0,0,0,16.576,14.034H495.2a16.805,16.805,0,0,0,16.8-16.8V152.928A16.807,16.807,0,0,0,495.2,136.123ZM130.253,358.7A90.748,90.748,0,1,1,221,267.954,90.851,90.851,0,0,1,130.253,358.7Zm251.494,0a90.748,90.748,0,1,1,90.748-90.748A90.851,90.851,0,0,1,381.747,358.7Z" />
            </g>
        </svg>
    </a>
    <div class="newSliderSection active">
        <?php print_r($package);?>
        @foreach($package as $packages)
        <div class="newSlide first" data-target="{{$packages->id}}" style="background-image: url({{url('/')}}/{{ $packages->tourimagename}})">
            <div class="content animatable fadeInUp">
                <h2>{{$packages->tour_name}}</h2>
                <h4>{{$packages->caption}}</h4>
                <a href="#" data-target="#people">Explore</a>
            </div>
        </div>
        @endforeach
        
    </div>
    <!-- // Other Areas -->
    <!-- Auto play Sound -->
    <audio id="main_audio" preload="auto" loop="loop">
        <source src="main_audio.mp3" type="audio/mpeg" />
        <source src="main_audio.ogg" type="audio/ogg" />
        <embed id="main_audio_ie8" hidden="true" loop="true" src="audio.mp3" />
    </audio>
    
    <!-- // Auto play Sound -->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/custom.js?ver=1"></script>
    <script src="js/three.min.js?ver=1"></script>
    <script type="text/javascript">
    // Background sound play pause
    var isPlaying, a = document.getElementById('main_audio');
    if (a.play instanceof Function) isPlaying = function(audio) {
        return !audio.paused;
    };
    else {
        a = document.getElementById('main_audio_ie8');
        isPlaying = function(audio) {
            return audio.playState == 3;
        };
        a.play = function() {
            this.controls.play();
        }
        a.pause = function() {
            this.controls.pause();
        }
    }
    document.getElementById('bars').onclick = function() {
        if (isPlaying(a)) {
            a.pause();
        } else {
            a.play();
        }
    };
    document.getElementById('otherAreaClick').onclick = function() {
        if (isPlaying(a)) {
            a.pause();
        } else {
            a.play();
        }
    };

    $('#bars').click(function() {
        $(this).find('.bar').toggleClass('enable');
    });
    // Drag Click
    $('.clickAndDrag a').click(function() {
        $(this).parents('.clickAndDragContainer').fadeOut();
    });
    $('.slideToggle').click(function() {
        $(this).toggleClass('active');
        $('.moreViewSlide, .backToMap, .social360').toggleClass('active');
    });
    // Other Area
    $('.otherAreaClick').click(function() {
        setTimeout(function() {
            $('.otherAreaClick').toggleClass('active');
        }, 500);
        $('.newSliderSection').fadeToggle();
        $('.newSliderSection').toggleClass('active');
    });
    $('.newSlide a').click(function() {
        setTimeout(function() {
            $('.otherAreaClick').toggleClass('active');
        }, 500);
        $('.newSliderSection').fadeToggle();
        $('.newSliderSection').toggleClass('active');
        if (isPlaying(a)) {
            a.pause();
        } else {
            a.play();
        }
    });

    $('.newSlide').mouseover(function() {
        var index = $(this).attr('data-target');
        if (index == 1) {
            $('.newSliderSection').css('background-image', 'url(images/area1.jpg)');
        }
        if (index == 2) {
            $('.newSliderSection').css('background-image', 'url(images/area2.jpg)');
        }
        if (index == 3) {
            $('.newSliderSection').css('background-image', 'url(images/area3.jpg)');
        }
        if (index == 4) {
            $('.newSliderSection').css('background-image', 'url(images/area4.jpg)');
        }
        $('.newSlide').addClass('noBg overlay');
        $(this).removeClass('overlay');
    });
    $('.newSlide').mouseout(function() {
        $('.newSlide').removeClass('noBg overlay');
    });
    $('.moreView').click(function() {
        $('.moreView').removeClass('active');
        $(this).addClass('active');
    });
    </script>
    <script>
    var manualControl = false;
    var longitude = 0;
    var latitude = 0;
    var savedX;
    var savedY;
    var savedLongitude;
    var savedLatitude;

    // panoramas background
    var panoramasArray = ["images/01.jpg", "images/02.jpg", "images/03.jpg", "images/04.jpg", "images/05.jpg"]
    var panoramaNumber = Math.floor(Math.random() * panoramasArray.length);

    // setting up the renderer
    renderer = new THREE.WebGLRenderer();
    renderer.setSize(window.innerWidth, window.innerHeight);
    document.body.appendChild(renderer.domElement);

    // creating a new scene
    var scene = new THREE.Scene();

    // adding a camera
    var camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 1, 1000);
    camera.target = new THREE.Vector3(0, 0, 0);

    // creation of a big sphere geometry
    var sphere = new THREE.SphereGeometry(100, 100, 40);
    sphere.applyMatrix(new THREE.Matrix4().makeScale(-1, 1, 1));

    // creation of the sphere material
    var sphereMaterial = new THREE.MeshBasicMaterial();
    sphereMaterial.map = THREE.ImageUtils.loadTexture(panoramasArray[panoramaNumber])

    // geometry + material = mesh (actual object)
    var sphereMesh = new THREE.Mesh(sphere, sphereMaterial);
    scene.add(sphereMesh);

    // listeners
    document.addEventListener("mousedown", onDocumentMouseDown, false);
    document.addEventListener("mousemove", onDocumentMouseMove, false);
    document.addEventListener("mouseup", onDocumentMouseUp, false);

    render();

    function render() {

        requestAnimationFrame(render);

        if (!manualControl) {
            longitude += 0.1;
        }

        // limiting latitude from -85 to 85 (cannot point to the sky or under your feet)
        latitude = Math.max(-85, Math.min(85, latitude));

        // moving the camera according to current latitude (vertical movement) and longitude (horizontal movement)
        camera.target.x = 500 * Math.sin(THREE.Math.degToRad(90 - latitude)) * Math.cos(THREE.Math.degToRad(longitude));
        camera.target.y = 500 * Math.cos(THREE.Math.degToRad(90 - latitude));
        camera.target.z = 500 * Math.sin(THREE.Math.degToRad(90 - latitude)) * Math.sin(THREE.Math.degToRad(longitude));
        camera.lookAt(camera.target);

        // calling again render function
        renderer.render(scene, camera);

    }

    // when the mouse is pressed, we switch to manual control and save current coordinates
    function onDocumentMouseDown(event) {

        event.preventDefault();

        manualControl = true;

        savedX = event.clientX;
        savedY = event.clientY;

        savedLongitude = longitude;
        savedLatitude = latitude;

    }

    // when the mouse moves, if in manual contro we adjust coordinates
    function onDocumentMouseMove(event) {

        if (manualControl) {
            longitude = (savedX - event.clientX) * 0.1 + savedLongitude;
            latitude = (event.clientY - savedY) * 0.1 + savedLatitude;
        }

    }

    // when the mouse is released, we turn manual control off
    function onDocumentMouseUp(event) {

        manualControl = false;

    }

    // pressing a key (actually releasing it) changes the texture map
    document.onkeyup = function(event) {

        panoramaNumber = (panoramaNumber + 1) % panoramasArray.length
        sphereMaterial.map = THREE.ImageUtils.loadTexture(panoramasArray[panoramaNumber])

    }
    </script>
@endsection