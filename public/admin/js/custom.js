$('#slider_f').validate({// initialize the plugin
    rules: {
        caption: {
            required: true
        },
        imagedata: {
            required: true

        }
    },
    messages: {
        caption: {
            required: "Please enter your caption"

        },
        imagedata: {
            required: "Please upload image"

        }
    },
    errorElement: "div",
    errorClass: "invalid",
    highlight: function (element, errorClass) {
        $(element).removeClass(errorClass);
    },
    onfocusout: function (element) {
        $(element).valid();
    },
    submitHandler: function () {

        var formData = new FormData();
        formData.append('caption', $('#caption').val());
        formData.append('filename', $('#filename').val());
        $('.overlay').show();
        $.ajax({
            url: base_url + '/updateslider',
             headers: {'X-CSRF-TOKEN': $('input[name=_token]').val()},
            type: "POST",
            async: true,
            cache: false,
            data: formData,
            contentType: false,
            processData: false,
            dataType: "json",
            success: function (data) {
              if(data.success == 1){
                  $('.successfull').show();
                  $('#slider_f')[0].reset();
                  
              } 
              else{
                  alert('Something Went Wrong');
              }
            }
        });


    }

});

function uploadimage(imagename) {

    var formData = new FormData();
    jQuery.each($('#' + imagename)[0].files, function (i, file) {
        formData.append(imagename, file);
    });

    

    $.ajax({
        url: base_url + '/uploadimages',
        headers: {'X-CSRF-TOKEN': $('input[name=_token]').val()},
        type: "POST",
        async: false,
        cache: false,
        data: formData,
        contentType: false,
        processData: false,
        dataType: "json",
        success: function (data) {

            if (data.msg === 'success') {
                $('.ext').hide();
                   var imageid = $('#' + imagename).parents('.inp1').find('.file_name').attr('id');
                $('#' + imageid).val(data.imagename);
                $('.filename').val(data.imagename);
                }
            else if (data.msg === 'imageerror') {

                $('.ext').show();
              
            } else if (data.success == -2) {
                alert(data.msg);
                return false;
            }
        }
    });
}

// Add Search Center

$('#research_f').validate({// initialize the plugin
    rules: {
        short_desc: {
            required: true
        },
        full_desc: {
            required: true

        }
    },
    messages: {
        short_desc: {
            required: "Please enter your short description"

        },
        full_desc: {
            required: "Please enter your full description"

        }
    },
    errorElement: "div",
    errorClass: "invalid",
    highlight: function (element, errorClass) {
        $(element).removeClass(errorClass);
    },
    onfocusout: function (element) {
        $(element).valid();
    },
    submitHandler: function () {

        var formData = new FormData();
        formData.append('short_desc', $('#short_desc').val());
        formData.append('full_desc', $('#full_desc').val());
      
        $.ajax({
            url: base_url + '/addresearchcenter',
             headers: {'X-CSRF-TOKEN': $('input[name=_token]').val()},
            type: "POST",
            async: true,
            cache: false,
            data: formData,
            contentType: false,
            processData: false,
            dataType: "json",
            success: function (data) {
              if(data.success == 1){
                  $('.successfull').show();
                  $('#research_f')[0].reset();
                  
              } 
              else{
                  alert('Something Went Wrong');
              }
            }
        });


    }

});

// Add Category 

$('#catgory_f').validate({// initialize the plugin
    rules: {
        catgeory_name: {
            required: true
        },
        catgeory_image: {
            required: true

        }
    },
    messages: {
        catgeory_name: {
            required: "Please enter your Category Name"

        },
        catgeory_image: {
            required: "Please upload Category Image"

        }
    },
    errorElement: "div",
    errorClass: "invalid",
    highlight: function (element, errorClass) {
        $(element).removeClass(errorClass);
    },
    onfocusout: function (element) {
        $(element).valid();
    },
    submitHandler: function () {

        var formData = new FormData();
        formData.append('category_name', $('#category_name').val());
        formData.append('category_image', $('#filename').val());
      
        $.ajax({
            url: base_url + '/savecategory',
             headers: {'X-CSRF-TOKEN': $('input[name=_token]').val()},
            type: "POST",
            async: true,
            cache: false,
            data: formData,
            contentType: false,
            processData: false,
            dataType: "json",
            success: function (data) {
              if(data.success == 1){
                  $('.successfull').show();
                  $('#catgory_f')[0].reset();
                  
              } 
              else{
                  alert('Something Went Wrong');
              }
            }
        });


    }

});

   // Add Product
   
   $('#product_f').validate({// initialize the plugin
    rules: {
        product_name: {
            required: true
        },
        product_image: {
            required: true

        }
    },
    messages: {
        product_name: {
            required: "Please enter your Product Name"

        },
        product_image: {
            required: "Please upload Product Image"

        }
    },
    errorElement: "div",
    errorClass: "invalid",
    highlight: function (element, errorClass) {
        $(element).removeClass(errorClass);
    },
    onfocusout: function (element) {
        $(element).valid();
    },
    submitHandler: function () {

        var formData = new FormData();
        formData.append('product_name', $('#product_name').val());
        formData.append('product_name', $('#filename').val());
        formData.append('family_name', $('#family_name').val());
        formData.append('synonyms', $('#synonyms').val());
        formData.append('local_name', $('#local_name').val());
        formData.append('english_name', $('#english_name').val());
        formData.append('hindi_name', $('#hindi_name').val());
        formData.append('medical_use', $('#medical_use').val());
        formData.append('brief_desc', $('#brief_desc').val());
        formData.append('distribution', $('#distribution').val());
       
        $.ajax({
            url: base_url + '/saveProduct',
             headers: {'X-CSRF-TOKEN': $('input[name=_token]').val()},
            type: "POST",
            async: true,
            cache: false,
            data: formData,
            contentType: false,
            processData: false,
            dataType: "json",
            success: function (data) {
              if(data.success == 1){
                  $('.successfull').show();
                  $('#product_f')[0].reset();
                  
              } 
              else{
                  alert('Something Went Wrong');
              }
            }
        });


    }

});

//Tour Details

 $('#tour_f').validate({// initialize the plugin
    rules: {
        title: {
            required: true
        },
        tour_image: {
            required: true

        }
    },
    messages: {
        title: {
            required: "Please enter your Product Name"

        },
        tour_image: {
            required: "Please upload Product Image"

        }
    },
    errorElement: "div",
    errorClass: "invalid",
    highlight: function (element, errorClass) {
        $(element).removeClass(errorClass);
    },
    onfocusout: function (element) {
        $(element).valid();
    },
    submitHandler: function () {

        var formData = new FormData();
        formData.append('title', $('#title').val());
        formData.append('tour_image', $('.filename').val());
        formData.append('description', $('#description').val());
        formData.append('time', $('#time').val());
      
       
        $.ajax({
            url: base_url + '/saveTour',
             headers: {'X-CSRF-TOKEN': $('input[name=_token]').val()},
            type: "POST",
            async: true,
            cache: false,
            data: formData,
            contentType: false,
            processData: false,
            dataType: "json",
            success: function (data) {
              if(data.success == 1){
                  $('.successfull').show();
                  $('#tour_f')[0].reset();
                  
              } 
              else{
                  alert('Something Went Wrong');
              }
            }
        });


    }

});

 //  Save tour Package
 
 $('#package_f').validate({// initialize the plugin
    rules: {
        tour_name: {
            required: true
        },
        tour_image: {
            required: true

        }
    },
    messages: {
        tour_name: {
            required: "Please enter your Product Name"

        },
        tour_image: {
            required: "Please upload Product Image"

        }
    },
    errorElement: "div",
    errorClass: "invalid",
    highlight: function (element, errorClass) {
        $(element).removeClass(errorClass);
    },
    onfocusout: function (element) {
        $(element).valid();
    },
    submitHandler: function () {

        var formData = new FormData();
        formData.append('tour_name', $('#tour_name').val());
        formData.append('threedimage', $('#threedimage').val());
        formData.append('tourimagename', $('#tourimagename').val());
        formData.append('caption', $('#caption').val());
       formData.append('main_tour', $('#main_tour').val());
       
        $.ajax({
            url: base_url + '/savePackage',
             headers: {'X-CSRF-TOKEN': $('input[name=_token]').val()},
            type: "POST",
            async: true,
            cache: false,
            data: formData,
            contentType: false,
            processData: false,
            dataType: "json",
            success: function (data) {
              if(data.success == 1){
                  $('.successfull').show();
                  $('#package_f')[0].reset();
                  
              } 
              else{
                  alert('Something Went Wrong');
              }
            }
        });


    }

});