$('.productImage img').click(function() {
    $(this).parent().toggleClass('active');
    $('.productInfo').toggleClass('active');
});

if ($(window).width() < 767) {
    var searchBox = $('.searchContainer').html();
    $('nav .container-fluid').append('<div class="searchContainer">' + searchBox + '</div>');
}
// Dropdown On hover
if ($(window).width() > 767) {
    $('ul.nav li.dropdown').hover(function() {
        $(this).addClass('open');
        $(this).find('.dropdown-menu').stop(true, true).fadeIn(500);
    }, function() {
        $(this).removeClass('open');
        $(this).find('.dropdown-menu').stop(true, true).fadeOut(500);
    });
}

if ($(window).width() > 767) {
    // Function which adds the 'animated' class to any '.animatable' in view
    var doAnimations = function() {
        // Calc current offset and get all animatables
        var offset = $(window).scrollTop() + $(window).height(),
            $animatables = $(".animatable");

        // Unbind scroll handler if we have no animatables
        if ($animatables.size() == 0) {
            $(window).off("scroll", doAnimations);
        }

        // Check all animatables and animate them if necessary
        $animatables.each(function(i) {
            var $animatable = $(this);
            if ($animatable.offset().top + $animatable.height() - 100 < offset) {
                $animatable.removeClass("animatable").addClass("animated");
            }
        });
    };

    // Hook doAnimations on scroll, and trigger a scroll
    $(window).on("scroll", doAnimations);
    $(window).trigger("scroll");
}


$('.youtubeClick').click(function() {
    $('.watchVideoCont').show();
    $('body').addClass('noScroll');
    $(".watchVideoCont iframe")[0].src += "&autoplay=1";
});

$('.popClose').click(function() {
    $('.watchVideoCont').hide();
    $(".watchVideoCont iframe").attr("src", 'https://www.youtube.com/embed/dIulcjcs_tw?rel=0&amp;showinfo=0?ecver=2');
    $('body').removeClass('noScroll');
});

function menuFix() {
    if ($('body').scrollTop() > 100) {
        $('.navbar-collapse').addClass('fixed');
        $('body').addClass('menufixed');
    } else {
        $('.navbar-collapse').removeClass('fixed');
        $('body').removeClass('menufixed');
    }
}
menuFix();
$(window).scroll(function() {
    menuFix();
});

$('.search, .searchBtn').click(function() {
    $('.navbar-nav').addClass('opacity0');
    $('.searchContainer').show();
    $('.searchFlex input').focus();

});
$('.searchFlex button.times').click(function() {
    $('.navbar-nav').removeClass('opacity0');
    $('.searchContainer').hide();
});

$('.navbar-toggle').click(function(){
    $(this).toggleClass('active');
});


