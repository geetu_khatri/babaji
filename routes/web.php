<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'WebsiteController@index')->name('index');
Route::get('/category', 'WebsiteController@category')->name('category');
Auth::routes();

Route::get('/home', 'AdminController@index')->name('home');
Route::get('/addslider', 'AdminController@addslider')->name('addslider');
Route::post('/updateslider', 'AdminController@updateslider')->name('updateslider');
Route::post('/uploadimages', 'AdminController@uploadimages')->name('uploadimages');
Route::get('/researchcenter', 'AdminController@researchcenter')->name('researchcenter');

Route::post('/addresearchcenter', 'AdminController@addresearchcenter')->name('addresearchcenter');
Route::get('/addcategory', 'AdminController@addcategory')->name('addcategory');
Route::post('/savecategory', 'AdminController@savecategory')->name('savecategory');
Route::get('/addProduct', 'AdminController@addProduct')->name('addProduct');
Route::get('/addTour', 'AdminController@addTour')->name('addTour');
Route::post('/saveTour', 'AdminController@saveTour')->name('saveTour');
Route::get('/addPackage', 'AdminController@addPackage')->name('addPackage');
Route::post('/savePackage', 'AdminController@savePackage')->name('savePackage');
Route::get('/360view/{id}', 'WebsiteController@threesixty')->name('360view/{id}');