<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;
use Session;
use Validator;
use Image;
use App\Website;
use Storage;
use App\Slider;
use App\Researchcenter;
use App\Category;
use App\Tour;
use App\Package;
class AdminController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return view('home');
    }

    public function addslider() {
        return view('admin/addslider');
    }

    public function updateslider(Request $request) {
        $slider = new slider;
        $slider->caption = Input::get('caption');
        $slider->imagename = Input::get('filename');
        $slider->save();
        $msg = 'added successfully!';
        $response = array('success' => 1, 'msg' => $msg);
       
        try {
            $slider->save();
        } catch (\Illuminate\Database\QueryException $e) {
            echo $e->getMessage();
            $response = array('success' => -1, 'msg' => 'Image not added into gallery something went wrong');
        }
        if ($response['success'] == 1) {
           
            return response()->json($response);
        } else {
            return response()->json($response);
        }
    }

    public function uploadimages() {
        $destinationPath = 'uploads'; // upload path
        foreach ($_FILES as $key => $value) {
            $keyname = $key;
        }

        if ($keyname) {
            // Build the input for validation
            $fileArray = array('image' => Input::file($keyname));

            // Tell the validator that this file should be an image
            $rules = array(
                'image' => 'mimes:jpeg,jpg,png,gif|required|max:10000' // max 10000kb
            );

            // Now pass the input and rules into the validator
            $validator = Validator::make($fileArray, $rules);

            // Check to see if validation fails or passes
            if ($validator->fails()) {
                $msg = "imageerror";
                echo json_encode(array('msg' => $msg));
            } else {
                $extension = Input::file($keyname)->getClientOriginalExtension(); // getting image extension
                $fileName = rand(11111, 99999) . '.' . $extension; // renameing image
                Input::file($keyname)->move($destinationPath, $fileName); // uploading file to given path
                $addharcard_image = $destinationPath . '/' . $fileName;
                $imagename = array('Imagename' => $addharcard_image);
                Session::put('imagename', $imagename);

                $msg = "success";
                echo json_encode(array('msg' => $msg, 'imagename' => $addharcard_image));
            }
        }
    }
    public function researchcenter() {
        return view('admin/addreasearchcenter');
    }
    
    public function addresearchcenter(Request $request){
          $researchcenter = new researchcenter;
        $researchcenter->short_desc = Input::get('short_desc');
        $researchcenter->full_desc = Input::get('full_desc');
        $researchcenter->save();
        $msg = 'added successfully!';
        $response = array('success' => 1, 'msg' => $msg);
       
        try {
            $researchcenter->save();
        } catch (\Illuminate\Database\QueryException $e) {
            echo $e->getMessage();
            $response = array('success' => -1, 'msg' => 'something went wrong');
        }
        if ($response['success'] == 1) {
           
            return response()->json($response);
        } else {
            return response()->json($response);
        }
    }
    
    public function addcategory(){
          return view('admin/addcategory');
          
    }
    public function savecategory(Request $request){
           $category = new category;
        $category->category_name = Input::get('category_name');
        $category->category_image = Input::get('category_image');
        $category->save();
        $msg = 'added successfully!';
        $response = array('success' => 1, 'msg' => $msg);
       
        try {
            $category->save();
        } catch (\Illuminate\Database\QueryException $e) {
            echo $e->getMessage();
            $response = array('success' => -1, 'msg' => 'Image not added into gallery something went wrong');
        }
        if ($response['success'] == 1) {
           
            return response()->json($response);
        } else {
            return response()->json($response);
        }
    }
    public function addProduct(){
        $category = DB::table('categories')->get();
          return view('admin/addProduct',compact('category'));
    }
    public function saveProduct(){
            $category = new category;
        $category->category_name = Input::get('category_name');
        $category->category_image = Input::get('category_image');
        $category->save();
        $msg = 'added successfully!';
        $response = array('success' => 1, 'msg' => $msg);
       
        try {
            $category->save();
        } catch (\Illuminate\Database\QueryException $e) {
            echo $e->getMessage();
            $response = array('success' => -1, 'msg' => 'Image not added into gallery something went wrong');
        }
        if ($response['success'] == 1) {
           
            return response()->json($response);
        } else {
            return response()->json($response);
        }
    }
    public function  addTour(){
          return view('admin/addTour');
          
    }
    
    public function saveTour(){
           $tour = new tour;
        $tour->title = Input::get('title');
        $tour->tour_image = Input::get('tour_image');
        $tour->time = Input::get('time');
        $tour->description = Input::get('description');
        $tour->save();
        $msg = 'added successfully!';
        $response = array('success' => 1, 'msg' => $msg);
       
        try {
            $tour->save();
        } catch (\Illuminate\Database\QueryException $e) {
            echo $e->getMessage();
            $response = array('success' => -1, 'msg' => 'Image not added into gallery something went wrong');
        }
        if ($response['success'] == 1) {
           
            return response()->json($response);
        } else {
            return response()->json($response);
        }
    }
    public function addPackage(){
         $tours = DB::table('tours')->get();
          return view('admin/addPackageExplore',  compact('tours'));
    }
    
    public function savePackage(){
        $package = new package;
        $package->tour_name = Input::get('tour_name');
        $package->caption = Input::get('caption');
        $package->tourimagename = Input::get('tourimagename');
        $package->threedimage = Input::get('threedimage');
        $package->save();
        $msg = 'added successfully!';
        $response = array('success' => 1, 'msg' => $msg);
       
        try {
            $package->save();
        } catch (\Illuminate\Database\QueryException $e) {
            echo $e->getMessage();
            $response = array('success' => -1, 'msg' => 'Image not added into gallery something went wrong');
        }
        if ($response['success'] == 1) {
           
            return response()->json($response);
        } else {
            return response()->json($response);
        }
    }
    

}
