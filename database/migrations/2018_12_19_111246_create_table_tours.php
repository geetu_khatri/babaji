<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTours extends Migration
{
    /**
     * Run the migrations.
     * $tour->title = Input::get('title');
        $tour->tour_image = Input::get('tour_image');
        $tour->time = Input::get('time');
        $tour->description = Input::get('description');
        $tour->save();
     * @return void
     */
   public function up()
    {
        Schema::create('tours', function (Blueprint $table) {
             $table->increments('id');
             $table->string('title','200');
             $table->string('tour_image','200');
             $table->string('time','200');
             $table->text('description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('tours');
    }
}
